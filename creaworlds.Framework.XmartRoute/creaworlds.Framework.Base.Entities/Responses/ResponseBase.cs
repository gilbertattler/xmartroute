﻿using creaworlds.Framework.Base.Enumerators.Responses;
using creaworlds.Framework.Base.Interfaces.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.Base.Entities.Responses
{
	[Serializable]
	public abstract class ResponseBase
	{
		#region Fields
		internal int? _intCodeNumber = (int)Codes.NotImplemented;
		internal Codes _objCodeName = Codes.NotImplemented;
		#endregion

		#region CodeNumber
		/// <summary>
		/// Identificador del Código de Respuesta.
		/// </summary>
		public int? CodeNumber
		{
			get
			{
				return this._intCodeNumber;
			}
			set
			{
				this._intCodeNumber = value;
				this._objCodeName = (Codes)this._intCodeNumber;
			}
		}
		#endregion

		#region CodeName
		/// <summary>
		/// Nombre del Códgio de Respuesta.
		/// </summary>
		public Codes CodeName
		{
			get
			{
				return this._objCodeName;
			}
			set
			{
				this._objCodeName = value;
				this._intCodeNumber = (int?)this._objCodeName;
			}
		}
		#endregion

		#region Message
		/// <summary>
		/// Mensaje resultado de la operación.
		/// </summary>
		public string Message { get; set; }
		#endregion

		#region ServerName
		public string ServerName
		{
			set { }
			get { return System.Environment.MachineName; }
		}
		#endregion

		#region ErrorNumber
		/// <summary>
		/// Número de Error en caso de existir.
		/// </summary>
		public int? ErrorNumber { get; set; }
		#endregion

		#region Constructor
		public ResponseBase()
		{
			this.CodeName = Codes.NotImplemented;
		}
		#endregion

		#region Configure
		public void Configure(Codes codeName)
		{
			this.CodeName = codeName;
		}

		public void Configure(int codeNumber)
		{
			this.CodeNumber = codeNumber;
		}

		public void Configure(Codes codeName, string message, int? errorNumber)
		{
			this.CodeName = codeName;
			this.Message = message;
			this.ErrorNumber = errorNumber;
		}

		public void Configure(int codeNumber, string message, int? errorNumber)
		{
			this.CodeNumber = codeNumber;
			this.Message = message;
			this.ErrorNumber = errorNumber;
		}
		#endregion

		#region ToResponse()
		public ResponseEmpty ToResponseEmpty()
		{
			return new ResponseEmpty()
			{
				CodeName = this.CodeName,
				CodeNumber = this.CodeNumber,
				Message = this.Message,
				ErrorNumber = this.ErrorNumber
			};
		}

		public ResponseObject<obj> ToResponseObject<obj>(ref obj result)
		{
			return new ResponseObject<obj>()
			{
				CodeName = this.CodeName,
				CodeNumber = this.CodeNumber,
				Message = this.Message,
				ErrorNumber = this.ErrorNumber,
				Result = result
			};
		}
		#endregion
	}
}

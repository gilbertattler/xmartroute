﻿using creaworlds.Framework.Base.Interfaces.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.Base.Entities.Responses
{
	public sealed class ResponseObject<obj> : ResponseBase, IResponse, IResultable<obj>
	{
		public obj Result { get; set; }
	}
}

﻿using creaworlds.Framework.Base.Interfaces.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.Base.Entities.Responses
{
	[Serializable]
	public sealed class ResponseEmpty : ResponseBase, IResponse
	{
		
	}
}

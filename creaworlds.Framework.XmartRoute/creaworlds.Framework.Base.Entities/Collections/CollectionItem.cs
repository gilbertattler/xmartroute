﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.Base.Entities.Collections
{
	public sealed class CollectionItem
	{
		public string ID { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }

		/// <summary>
		/// Utilizado para transferir información adicional
		/// </summary>
		public string Tag { get; set; }
		public bool? IsActive { get; set; }
	}
}

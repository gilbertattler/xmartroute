﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.Base.Entities.Collections
{
	[Serializable]
	public class DictionaryItem
	{
		public string Key { get; set; }
		public string Value { get; set; }
	}
}

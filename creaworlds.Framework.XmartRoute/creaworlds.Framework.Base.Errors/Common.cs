﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.Base.Errors
{
	public static partial class Common
	{
		public const string ERR_COMMON_NULLREQUEST = "No se han especificado los parametros requeridos para realizar la operación o son invalidos.";
		public const string ERR_COMMON_NULLCONTENT = "No se han especificado el contenido de la solicitud es invalida.";
		public const string ERR_COMMON_AUTHORID = "No se ha especificado el ID del usuario que realiza la operación o es invalido.";
		public const string ERR_COMMON_COMPANYID = "No se ha especificado el ID del usuario que realiza la operación o es invalido.";
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.Base.Errors
{
	public static partial class DataBase
	{
		public const string ERR_DATABASE_OPENCONNECTION = "Ha ocurrido un error al conectar con la capa de datos.";
		public const string ERR_DATABASE_TRANOPEN = "Ha ocurrido un error al iniciar la operación con la capa de datos.";
		public const string ERR_DATABASE_TRANCOMMIT = "Ha ocurrido un error al confirmar la operación con la capa de datos.";
	}
}

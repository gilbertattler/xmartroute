﻿using System;
using System.Data;
using System.Data.OleDb;

namespace creaworlds.Framework.Base.Tools
{
	public static class OleDB
	{
		#region GetStringConnection
		internal static string GetStringConnection(string fileName, bool isHDR)
		{
			return string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR={1};IMEX=1;TypeGuessRows=0"";", fileName, (isHDR ? "YES" : "NO"));
		}
		#endregion

		#region OpenConnection
		public static OleDbConnection OpenConnection(string filePath, bool isHDR)
		{
			OleDbConnection conn = null;

			try
			{
				conn = new OleDbConnection(OleDB.GetStringConnection(filePath, isHDR));
				conn.Disposed += conn_Disposed;
				conn.Open();
			}
			catch (Exception ex)
			{
				Diagnostics.OnError(new Exception("Ha ocurrido un error al conectar con el archivo indicado.", ex));
				conn = null;
			}

			return conn;
		}

		internal static void conn_Disposed(object sender, EventArgs e)
		{
			OleDbCommand cmd = (OleDbCommand)sender;

			if (cmd.Connection != null)
			{
				cmd.Connection.Close();
			}
		}
		#endregion

		#region GetTables
		public static DataTable GetTables(string filePath, bool isHDR)
		{
			bool done = false;
			DataTable dt = null;

			using (OleDbConnection conn = OleDB.OpenConnection(filePath, isHDR))
			{
				if (conn != null)
				{
					try
					{
						dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
						done = (dt.Rows.Count > 0);
					}
					catch (Exception ex)
					{
						Diagnostics.OnError(new Exception("Ha ocurrido un error al obtener las tablas del archivo indicado.", ex));
						dt = null;
					}
				}
			}

			return dt;
		}
		#endregion

		#region GetColumnNames
		public static DataTable GetColumnNames(string filePath, bool isHDR, string SheetName)
		{
			bool done = false;
			DataTable dt = null;

			using (OleDbConnection conn = OleDB.OpenConnection(filePath, isHDR))
			{
				if (conn != null)
				{
					try
					{
						dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, SheetName, null });
						done = (dt.Rows.Count > 0);
					}
					catch (Exception ex)
					{
						Diagnostics.OnError(new Exception("Ha ocurrido un error al obtener las columnas del archivo indicado.", ex));
						dt = null;
					}
				}
			}

			return dt;
		}
		#endregion
	}
}

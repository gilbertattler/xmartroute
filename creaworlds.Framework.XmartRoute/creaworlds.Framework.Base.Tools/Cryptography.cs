﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace creaworlds.Framework.Base.Tools
{
	public static class Cryptography
	{
		#region MD5
		public static string MD5(string str)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			return BitConverter.ToString(md5.ComputeHash(ASCIIEncoding.Default.GetBytes(str))).Replace("-", "");
		}
		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace creaworlds.Framework.Base.Tools
{
	public static class Validator
	{
		#region IsGUID
		public static bool IsGuid(string text)
		{
			Guid tmp;
			return Guid.TryParse(text, out tmp);
		}
		#endregion

		#region IsNumber
		public static bool IsNumber(string text)
		{
			long numberConverted;
			return long.TryParse(text, out numberConverted);
		}
		#endregion

		#region IsIPAddress
		public static bool IsIPAddress(string text)
		{
			bool done = false;

			try
			{
				IPAddress.Parse(text);
				done = true;
			}
			catch (Exception ex)
			{

			}

			return done;
		}
		#endregion

		#region IsEmail
		public static bool IsEmail(string emailAddress)
		{
			return !string.IsNullOrWhiteSpace(emailAddress) && Regex.IsMatch(emailAddress, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
		}
		#endregion

		#region IsPhoneNumber
		public static bool IsPhoneNumber(string areaCode, string phoneNumber)
		{
			return !string.IsNullOrWhiteSpace(areaCode) &&
				!string.IsNullOrWhiteSpace(phoneNumber) &&
				string.Format("{0}{1}", areaCode, phoneNumber).Length == 10;
		}

		public static bool IsValidNotRequiredPhoneNumber(string areaCode, string phoneNumber)
		{
			bool done = true;

			if (!string.IsNullOrWhiteSpace(areaCode) || string.IsNullOrWhiteSpace(phoneNumber))
			{
				done = Validator.IsPhoneNumber(areaCode, phoneNumber);
			}

			return done;

		}
		#endregion

		#region String[] Empty Entries
		public static bool ContainsEmptyEntries(string[] o)
		{
			bool found = false;

			foreach (string i in o)
			{
				if (string.IsNullOrWhiteSpace(i))
				{
					found = true;
					break;
				}
			}

			return found;
		}

		public static string[] RemoveEmptyEntries(string[] o)
		{
			List<string> tmp = new List<string>();

			foreach (string i in o)
			{
				if (!string.IsNullOrWhiteSpace(i))
				{
					tmp.Add(i);
				}
			}

			return tmp.ToArray();
		}
		#endregion
	}
}

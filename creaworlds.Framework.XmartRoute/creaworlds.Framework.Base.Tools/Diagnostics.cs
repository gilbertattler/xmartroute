﻿using System;
using System.Configuration;
using System.IO;

namespace creaworlds.Framework.Base.Tools
{
	public static class Diagnostics
	{
		#region OnError
		public static void OnError(Exception ex)
		{
			try
			{
				string path = string.Format(@"{0}\{1}.log", ConfigurationManager.AppSettings["PATH_LOG"], DateTime.Now.Ticks);

				using (StreamWriter sw = new StreamWriter(new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None), System.Text.Encoding.UTF8))
				{
					sw.WriteLine(ex);
				}
			}
			catch (Exception)
			{
				throw ex;
			}
		}
		#endregion
	}
}

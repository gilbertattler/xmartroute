﻿using creaworlds.Framework.Base.Interfaces.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.Base.Tools
{
	public static class Responses
	{
		#region Web Responses
		public static string ConfigureWebResponse(IResponse response)
		{
			return string.Format("{0}/{1}<br/>{2}<br/>Error: {3}", response.CodeNumber, response.CodeName, response.Message, response.ErrorNumber);
		}

		public static string ConfigureExceptionResponse(IResponse response)
		{
			return string.Format("{0}/{1} {2}, Error: {3}", response.CodeNumber, response.CodeName, response.Message, response.ErrorNumber);
		}
		#endregion

		#region Data Access Responses
		public static void ConfigureNullRequest(IResponse response)
		{
			response.Configure(Enumerators.Responses.Codes.ServiceUnavailable, Errors.Common.ERR_COMMON_NULLREQUEST, 1500);
		}

		public static void ConfigureNullContent(IResponse response)
		{
			response.Configure(Enumerators.Responses.Codes.ServiceUnavailable, Errors.Common.ERR_COMMON_NULLCONTENT, 1500);
		}

		public static void ConfigureConnectionFailure(IResponse response)
		{
			response.Configure(Enumerators.Responses.Codes.ServiceUnavailable, Errors.DataBase.ERR_DATABASE_OPENCONNECTION, 1500);
		}

		public static void ConfigureTranBeginFailure(IResponse response)
		{
			response.Configure(Enumerators.Responses.Codes.ServiceUnavailable, Errors.DataBase.ERR_DATABASE_TRANOPEN, 1501);
		}

		public static void ConfigureTranCommitFailure(IResponse response)
		{
			response.Configure(Enumerators.Responses.Codes.InternalServerError, Errors.DataBase.ERR_DATABASE_TRANCOMMIT, 1502);
		}

		public static void ConfigureAuthorNotSpecified(IResponse response)
		{
			response.Configure(Enumerators.Responses.Codes.BadRequest, Errors.Common.ERR_COMMON_AUTHORID, 1400);
		}

		public static void ConfigureCompanyNotSpecified(IResponse response)
		{
			response.Configure(Enumerators.Responses.Codes.BadRequest, Errors.Common.ERR_COMMON_COMPANYID, 1400);
		}
		#endregion
	}
}

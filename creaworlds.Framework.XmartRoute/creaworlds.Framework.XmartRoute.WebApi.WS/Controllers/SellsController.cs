﻿using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Sells;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Web.Mvc;

namespace creaworlds.Framework.XmartRoute.WebApi.WS.Controllers
{
    public class SellsController : Controller
    {
        // GET: Sells
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UploadSells(RequestObject<SellUpload> request)
        {
            ResponseObject<SellData[]> response = new ResponseObject<SellData[]>();

            try
            {
                response = DataAccess.Sells.SellUpload(ref request);
            }
            catch (Exception ex)
            {
                Base.Tools.Diagnostics.OnError(ex);
            }

            return Json(response);
        }
    }
}
﻿using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Customers;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Web.Mvc;


namespace creaworlds.Framework.XmartRoute.WebApi.WS.Controllers
{
    public class CustomersController : Controller
    {
        // GET: Customers
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UploadCustomers(RequestObject<CustomerUpload> request)
        {
            ResponseObject<CustomerData[]> response = new ResponseObject<CustomerData[]>();

            try
            {
                response = DataAccess.Customers.CustomerUpload(ref request);
            }
            catch (Exception ex)
            {
                Base.Tools.Diagnostics.OnError(ex);
            }

            return Json(response);
        }
    }
}
﻿using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Customers;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Web.Mvc;


namespace creaworlds.Framework.XmartRoute.WebApi.WS.Controllers
{
    public class StoresController : Controller
    {
        // GET: Stores
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UploadStores(RequestObject<StoreUpload> request)
        {
            ResponseObject<StoreData[]> response = new ResponseObject<StoreData[]>();

            try
            {
                response = DataAccess.Customers.StoreUpload(ref request);
            }
            catch (Exception ex)
            {
                Base.Tools.Diagnostics.OnError(ex);
            }

            return Json(response);
        }
    }
}
﻿using creaworlds.Framework.Base.Entities.Collections;
using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace creaworlds.Framework.XmartRoute.WebApi.WS.Controllers
{
    public class CollectionsController : Controller
    {
        // GET: Collections
		[HttpPost]
        public JsonResult ListActivities(RequestEmpty request)
        {
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			try
			{
				response = DataAccess.Catalogs.ListActivities(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
        }

		[HttpPost]
		public JsonResult ListPriceList(RequestEmpty request)
		{
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			try
			{
				response = DataAccess.Catalogs.ListPriceList(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult ListStates(RequestEmpty request)
		{
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			try
			{
				response = DataAccess.Catalogs.ListStates(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult ListTaxes(RequestEmpty request)
		{
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			try
			{
				response = DataAccess.Catalogs.ListTaxes(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult ListFrequencies(RequestEmpty request)
		{
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			try
			{
				response = DataAccess.Catalogs.ListFrequencies(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}
	}
}
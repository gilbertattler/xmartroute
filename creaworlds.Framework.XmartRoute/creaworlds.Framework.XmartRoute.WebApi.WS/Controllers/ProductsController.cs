﻿using creaworlds.Framework.Base.Entities.Collections;
using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Products;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace creaworlds.Framework.XmartRoute.WebApi.WS.Controllers
{
	public class ProductsController : Controller
	{
		// GET: Produts
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public JsonResult CategoriesList(RequestEmpty request)
		{
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			try
			{
				response = DataAccess.Products.CategoriesList(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult BrandsList(RequestEmpty request)
		{
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			try
			{
				response = DataAccess.Products.BrandsList(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult UnitsList(RequestEmpty request)
		{
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			try
			{
				response = DataAccess.Products.UnitsList(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult List(RequestEmpty request)
		{
			ResponseObject<ProductData[]> response = new ResponseObject<ProductData[]>();

			try
			{
				response = DataAccess.Products.List(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult ArticleList(RequestEmpty request)
		{
			ResponseObject<ArticleData[]> response = new ResponseObject<ArticleData[]>();

			try
			{
				response = DataAccess.Products.ArticleList(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult TaxesList(RequestEmpty request)
		{
			ResponseObject<ProductTaxData[]> response = new ResponseObject<ProductTaxData[]>();

			try
			{
				response = DataAccess.Products.TaxesList(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult DownloadStock(RequestEmpty request)
		{
			ResponseObject<StockData[]> response = new ResponseObject<StockData[]>();

			try
			{
				response = DataAccess.Products.StockList(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult DownloadSellPrices(RequestEmpty request)
		{
			ResponseObject<PriceData[]> response = new ResponseObject<PriceData[]>();

			try
			{
				response = DataAccess.Products.ListSellPrices(ref request);
			}
			catch (Exception ex)
			{
				Base.Tools.Diagnostics.OnError(ex);
			}

			return Json(response);
		}
	}
}
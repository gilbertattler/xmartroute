﻿using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Customers;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.DataAccess
{
    public static partial class Customers
    {
        public static ResponseObject<CustomerData[]> CustomerUpload(ref RequestObject<CustomerUpload> request)
        {
            SqlCommand cmd = null;
            ResponseObject<CustomerData[]> response = new ResponseObject<CustomerData[]>();

            if (Validators.VerifyIntegrity(request, response))
            {
                if (request.Content == null)
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la información del cliente.", 2000);
                }
                else if (!Base.Tools.Validator.IsGuid(request.Content.ID))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el ID del cliente o es invalido.", 2001);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.Name))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el nombre del negocio.", 2002);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.LegalName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la Razón Social del cliente.", 2003);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.Address))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la dirección del cliente.", 2004);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.CityName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la ciudad del cliente.", 2005);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.StateName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el estado del cliente.", 2006);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.CountryName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el país del cliente.", 2007);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.eMail))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el email del cliente.", 2008);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.ManagerName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el nombre del Dueño Gerente o Encargado.", 2009);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.Neighborhood))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la colonia del cliente.", 2010);
                }
                else if (request.Content.PhoneMobile == null)
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado Teléfono Móvil del cliente.", 2011);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.PhoneWork))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el Teléfono de Negocio del cliente.", 2012);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.TaxID))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el RFC del cliente.", 2013);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.ZipCode))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el Código Postal del cliente.", 2014);
                }
                else
                {
                    if (Base.Tools.DataBase.OpenConnection(ref cmd))
                    {
                        Base.Tools.Responses.ConfigureConnectionFailure(response);
                    }
                    else
                    {
                        try
                        {
                            cmd.CommandText = "SP_API_CUSTOMERS_UPDATE";
                            cmd.Parameters.Add("data", SqlDbType.Xml).Value = Base.Tools.Common.Serializer(request);

                            using (DataSet ds = new DataSet())
                            {
                                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                                {
                                    da.Fill(ds);
                                }

                                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsDone"]))
                                {
                                    if (ds.Tables[1].Rows.Count > 0)
                                    {
                                        response.Result = Base.Tools.Common.Populate<CustomerData>(ds.Tables[1]);
                                        response.Configure(Base.Enumerators.Responses.Codes.Found);
                                    }
                                    else
                                    {
                                        response.Result = new CustomerData[0];
                                        response.Configure(Base.Enumerators.Responses.Codes.NotFound);
                                    }
                                }
                                else
                                {
                                    response = Base.Tools.Common.Populate<ResponseObject<CustomerData[]>>(ds.Tables[0])[0];
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            response.Configure(Base.Enumerators.Responses.Codes.InternalServerError, "Ha ocurrido un error en el cargado del Cliente.", 2015);
                            Base.Tools.Diagnostics.OnError(new Exception(response.Message, ex));
                        }
                        finally
                        {
                            Base.Tools.DataBase.CloseConnection(ref cmd);
                        }
                    }
                }
            }

            return response;
        }
    }
}

﻿using creaworlds.Framework.Base.Entities.Collections;
using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Data;
using System.Data.SqlClient;

namespace creaworlds.Framework.XmartRoute.WebApi.DataAccess
{
	public static partial class Catalogs
	{
		public static ResponseObject<CollectionItem[]> ListFrequencies(ref RequestEmpty request)
		{
			SqlCommand cmd = null;
			ResponseObject<CollectionItem[]> response = new ResponseObject<CollectionItem[]>();

			if (Validators.VerifyIntegrity(request, response))
			{
				if (!Base.Tools.DataBase.OpenConnection(ref cmd))
				{
					Base.Tools.Responses.ConfigureConnectionFailure(response);
				}
				else
				{
					try
					{
						cmd.CommandText = "SP_API_CATALOGO_FREQVISITA_LISTA";
						cmd.Parameters.Add("data", SqlDbType.Xml).Value = Base.Tools.Common.Serializer(request);

						using (DataSet ds = new DataSet())
						{
							using (SqlDataAdapter da = new SqlDataAdapter(cmd))
							{
								da.Fill(ds);
							}

							if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsDone"]))
							{
								if (ds.Tables[1].Rows.Count > 0)
								{
									response.Result = Base.Tools.Common.Populate<CollectionItem>(ds.Tables[1]);
									response.Configure(Base.Enumerators.Responses.Codes.Found);
								}
								else
								{
									response.Result = new CollectionItem[0];
									response.Configure(Base.Enumerators.Responses.Codes.NotFound);
								}
							}
							else 
							{
								response = Base.Tools.Common.Populate<ResponseObject<CollectionItem[]>>(ds.Tables[0])[0];
							}
						}
					}
					catch (Exception ex)
					{
						response.Configure(Base.Enumerators.Responses.Codes.InternalServerError, "Ha ocurrido un error al obtener el listado de frecuencias de visita.", 1500);
						Base.Tools.Diagnostics.OnError(new Exception(response.Message, ex));
					}
					finally
					{
						Base.Tools.DataBase.CloseConnection(ref cmd);
					}
				}
			}

			return response;
		}
	}
}

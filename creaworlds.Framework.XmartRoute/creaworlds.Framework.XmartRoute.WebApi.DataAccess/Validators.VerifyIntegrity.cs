﻿using creaworlds.Framework.Base.Interfaces.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Interfaces.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.DataAccess
{
	public static partial class Validators
	{
		public static bool VerifyIntegrity(IRequest request, IResponse response)
		{
			bool isValid = false;

			if (response == null)
			{
				throw new ArgumentNullException("response", "No se ha especificado el argumento IResponse o es invalido.");
			}
			else if (request == null)
			{
				Base.Tools.Responses.ConfigureNullRequest(response);
			}
			else if (string.IsNullOrWhiteSpace(request.UserName))
			{
				response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la clave de acceso al api o es incorrecta.", 1401);
			}
			else if (string.IsNullOrWhiteSpace(request.Password))
			{
				response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la contraseña de acceso al api o es incorrecta.", 1402);
			}
			else if (string.IsNullOrWhiteSpace(request.Signature))
			{
				response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la firma electrónica del api o es incorrecta.", 1403);
			}
			else
			{
				isValid = true;
			}

			return isValid;
		}
	}
}

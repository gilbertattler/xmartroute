﻿using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Sells;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using creaworlds.Framework.Base.Entities.Collections;
using System;
using System.Data;
using System.Data.SqlClient;

namespace creaworlds.Framework.XmartRoute.WebApi.DataAccess
{
	public static partial class Sells
	{
		public static ResponseObject<SellData[]> SellUpload(ref RequestObject<SellUpload> request)
		{
			SqlCommand cmd = null;
			ResponseObject<SellData[]> response = new ResponseObject<SellData[]>();

			if (Validators.VerifyIntegrity(request, response))
			{

				if (request.Content == null)
				{
					response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la información de la venta.", 2040);
				}
				else if (!Base.Tools.Validator.IsGuid(request.Content.ID))
				{
					response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el ID de la venta.", 2041);
				}
				else if (!Base.Tools.Validator.IsGuid(request.Content.CustomerID))
				{
					response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el ID del cliente.", 2042);
				}
				else if (!Base.Tools.Validator.IsGuid(request.Content.StoreID))
				{
					response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el ID de la tienda", 2043);
				}
				else
				{
					bool isOk = true;

					foreach (DetailUpload item in request.Content.Items)
					{
						if (!Base.Tools.Validator.IsGuid(item.ArticleID))
						{
							response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el id de uno o más articulos de la venta.", 2044);
							isOk = false;
							break;
						}
						else if (!item.Amount.HasValue || item.Amount.Value <= 0)
						{
							response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la cantidad en uno o más articulos de la venta.", 2045);
							isOk = false;
							break;
						}
						else if (!item.RegularPrice.HasValue || item.RegularPrice.Value <= 0)
						{
							response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el precio base de uno o más articulos de la venta.", 2046);
							isOk = false;
							break;
						}
						else if (!item.FinalPrice.HasValue || item.FinalPrice.Value <= 0)
						{
							response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el precio final de uno o más articulos de la venta.", 2047);
							isOk = false;
							break;
						}
						else if (!item.Profitability.HasValue || item.Profitability.Value <= 0)
						{
							response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la rentabilidad de uno o más articulos de la venta.", 2048);
							isOk = false;
							break;
						}
					}

					if (isOk)
					{
						if (Base.Tools.DataBase.OpenConnection(ref cmd))
						{
							Base.Tools.Responses.ConfigureConnectionFailure(response);
						}
						else
						{
							try
							{
								cmd.CommandText = "SP_API_VENTAS_UPLOAD";
								cmd.Parameters.Add("data", SqlDbType.Xml).Value = Base.Tools.Common.Serializer(request);

								using (DataSet ds = new DataSet())
								{
									using (SqlDataAdapter da = new SqlDataAdapter(cmd))
									{
										da.Fill(ds);
									}

									if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsDone"]))
									{
										if (ds.Tables[1].Rows.Count > 0)
										{
											response.Result = Base.Tools.Common.Populate<SellData>(ds.Tables[1]);
											response.Configure(Base.Enumerators.Responses.Codes.Found);
										}
										else
										{
											response.Result = new SellData[0];
											response.Configure(Base.Enumerators.Responses.Codes.NotFound);
										}
									}
									else
									{
										response = Base.Tools.Common.Populate<ResponseObject<SellData[]>>(ds.Tables[0])[0];
									}
								}
							}
							catch (Exception ex)
							{
								response.Configure(Base.Enumerators.Responses.Codes.InternalServerError, "Ha ocurrido un error en la carga de las ventas.", 2047);
								Base.Tools.Diagnostics.OnError(new Exception(response.Message, ex));
							}
							finally
							{
								Base.Tools.DataBase.CloseConnection(ref cmd);
							}
						}
					}
				}


			}
			return response;
		}
	}
}

﻿using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Customers;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Data;
using System.Data.SqlClient;

namespace creaworlds.Framework.XmartRoute.WebApi.DataAccess
{
    public static partial class Customers
    {
        public static ResponseObject<StoreData[]> StoreUpload(ref RequestObject<StoreUpload> request)
        {
            SqlCommand cmd = null;
            ResponseObject<StoreData[]> response = new ResponseObject<StoreData[]>();
            if (Validators.VerifyIntegrity(request, response))
            {
                if (request.Content == null)
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la información del cliente.", 3000);
                }
                if (!Base.Tools.Validator.IsGuid(request.Content.ID))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el ID de la tienda o es invalido.", 3015);
                }
                else if (!Base.Tools.Validator.IsGuid(request.Content.ParentID))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado su ID del cliente.", 3016);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.Name))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el nombre del negocio.", 3017);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.Address))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la dirección de la tienda.", 3018);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.CityName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la ciudad de la tienda.", 3019);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.StateName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el estado de la tienda.", 3020);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.CountryName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el país de la tienda.", 3021);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.eMail))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el email de la tienda.", 3022);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.ManagerName))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el nombre del Dueño Gerente o Encargado de la tienda.", 3023);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.Neighborhood))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la colonia de la tienda.", 3024);
                }
                else if (request.Content.PhoneMobile == null)
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado Teléfono Móvil de la tienda.", 3025);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.PhoneWork))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el Teléfono de Negocio de la tienda.", 3026);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.Refers))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado las referencias de la tienda.", 3027);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.ZipCode))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el Código Postal de la tienda.", 3027);
                }
                else if (string.IsNullOrWhiteSpace(request.Content.VisitingDay))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado el día de visita de la tienda.", 3028);
                }
                else if (!Base.Tools.Validator.IsGuid(request.Content.FrequencyId))
                {
                    response.Configure(Base.Enumerators.Responses.Codes.BadRequest, "No se ha especificado la frecuencia de visita.", 3029);
                }
                else
                {
                    if (Base.Tools.DataBase.OpenConnection(ref cmd))
                    {
                        Base.Tools.Responses.ConfigureConnectionFailure(response);
                    }
                    else
                    {
                        try
                        {
                            cmd.CommandText = "SP_API_CUSTOMERS_UPDATE";
                            cmd.Parameters.Add("data", SqlDbType.Xml).Value = Base.Tools.Common.Serializer(request);

                            using (DataSet ds = new DataSet())
                            {
                                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                                {
                                    da.Fill(ds);
                                }

                                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsDone"]))
                                {
                                    if (ds.Tables[1].Rows.Count > 0)
                                    {
                                        response.Result = Base.Tools.Common.Populate<StoreData>(ds.Tables[1]);
                                        response.Configure(Base.Enumerators.Responses.Codes.Found);
                                    }
                                    else
                                    {
                                        response.Result = new StoreData[0];
                                        response.Configure(Base.Enumerators.Responses.Codes.NotFound);
                                    }
                                }
                                else
                                {
                                    response = Base.Tools.Common.Populate<ResponseObject<StoreData[]>>(ds.Tables[0])[0];
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            response.Configure(Base.Enumerators.Responses.Codes.InternalServerError, "Ha ocurrido un error en el cargado de la tienda.", 3030);
                            Base.Tools.Diagnostics.OnError(new Exception(response.Message, ex));
                        }
                        finally
                        {
                            Base.Tools.DataBase.CloseConnection(ref cmd);
                        }
                    }
                }
            }

            return response;
        }
    }
}

﻿using creaworlds.Framework.Base.Entities.Responses;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Products;
using creaworlds.Framework.XmartRoute.WebApi.Entities.Requests;
using System;
using System.Data;
using System.Data.SqlClient;

namespace creaworlds.Framework.XmartRoute.WebApi.DataAccess
{
	public static partial class Products
	{
		public static ResponseObject<ArticleData[]> ArticleList(ref RequestEmpty request)
		{
			SqlCommand cmd = null;
			ResponseObject<ArticleData[]> response = new ResponseObject<ArticleData[]>();

			if (Validators.VerifyIntegrity(request, response))
			{
				if (!Base.Tools.DataBase.OpenConnection(ref cmd))
				{
					Base.Tools.Responses.ConfigureConnectionFailure(response);
				}
				else
				{
					try
					{
						cmd.CommandText = "SP_API_PRODUCTO_ARTICULO_LISTA";
						cmd.Parameters.Add("data", SqlDbType.Xml).Value = Base.Tools.Common.Serializer(request);

						using (DataSet ds = new DataSet())
						{
							using (SqlDataAdapter da = new SqlDataAdapter(cmd))
							{
								da.Fill(ds);
							}

							if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsDone"]))
							{
								if (ds.Tables[1].Rows.Count > 0)
								{
									response.Result = Base.Tools.Common.Populate<ArticleData>(ds.Tables[1]);
									response.Configure(Base.Enumerators.Responses.Codes.Found);
								}
								else
								{
									response.Result = new ArticleData[0];
									response.Configure(Base.Enumerators.Responses.Codes.NotFound);
								}
							}
							else 
							{
								response = Base.Tools.Common.Populate<ResponseObject<ArticleData[]>>(ds.Tables[0])[0];
							}
						}
					}
					catch (Exception ex)
					{
						response.Configure(Base.Enumerators.Responses.Codes.InternalServerError, "Ha ocurrido un error al obtener el listado de articulos de productos.", 1500);
						Base.Tools.Diagnostics.OnError(new Exception(response.Message, ex));
					}
					finally
					{
						Base.Tools.DataBase.CloseConnection(ref cmd);
					}
				}
			}

			return response;
		}
	}
}

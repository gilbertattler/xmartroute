// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// "starter" is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of "requires"
// "starter.controllers" is found in controllers.js
var app = angular.module("xmartRoute", ["ionic", "appControllers", "appServices", "appDataBase"])

.run(function ($ionicPlatform, $log) {
	$ionicPlatform.ready(function () {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});
})

.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state("splash", {
		url: "/splash",
		templateUrl: "templates/splash.html",
		controller: "splashController"
	});

	$stateProvider.state("app", {
		url: "/app",
		abstract: true,
		templateUrl: "templates/default.html",
		controller: "defaultController"
	});

	$stateProvider.state("app.settings", {
		url: "/settings",
		views: {
			"menuContent": {
				templateUrl: "templates/settings.html",
				controller: "settingsController"
			}
		}
	});

	$stateProvider.state("app.customers", {
		url: "/customers",
		views: {
			"menuContent": {
				templateUrl: "templates/customers.html",
				controller: "customerSearchController"
			}
		}
	});

	$stateProvider.state("app.customers-edit", {
		url: "/customers-edit/{ID}",
		views: {
			"menuContent": {
				templateUrl: "templates/customers-edit.html",
				controller: "customersController"
			}
		}
	});

	$stateProvider.state("app.stores", {
		url: "/stores/{CustomerID}",
		views: {
			"menuContent": {
				templateUrl: "templates/stores.html",
				controller: "storesController"
			}
		}
	});

	$stateProvider.state("app.stores-new", {
		url: "/stores-edit/CustomerID={CustomerID}",
		views: {
			"menuContent": {
				templateUrl: "templates/stores-edit.html",
				controller: "storesEditController"
			}
		}
	});

	$stateProvider.state("app.stores-edit", {
		url: "/stores-edit/ID={ID}",
		views: {
			"menuContent": {
				templateUrl: "templates/stores-edit.html",
				controller: "storesEditController"
			}
		}
	});

	$stateProvider.state("app.stores-sell", {
		url: "/order-edit/StoreID={StoreID}",
		views: {
			"menuContent": {
				templateUrl: "templates/order-edit.html",
				controller: "orderEditController"
			}
		}
	});

	$stateProvider.state("app.orders-show", {
		url: "/orders-show/ID={OrderID}",
		views: {
			"menuContent": {
				templateUrl: "templates/order-show.html",
				controller: "orderShowController"
			}
		}
	});

	$stateProvider.state("app.orders", {
		url: "/orders",
		views: {
			"menuContent": {
				templateUrl: "templates/orders.html",
				controller: "ordersController"
			}
		}
	});

	$stateProvider.state("app.customerOrders", {
		url: "/customerOrders/{CustomerID}",
		views: {
			"menuContent": {
				templateUrl: "templates/customerOrders.html",
				controller: "customerOrdersController"
			}
		}
	});

	$stateProvider.state("app.storeOrders", {
		url: "/storeOrders/{StoreID}",
		views: {
			"menuContent": {
				templateUrl: "templates/storeOrders.html",
				controller: "storeOrdersController"
			}
		}
	});

	$stateProvider.state("app.routes", {
		url: "/routes",
		views: {
			"menuContent": {
				templateUrl: "templates/routes.html",
				controller: "routesController"
			}
		}
	});

	$stateProvider.state("app.invoices", {
		url: "/invoices",
		views: {
			"menuContent": {
				templateUrl: "templates/invoices.html",
				controller: "invoicesController"
			}
		}
	});

	$stateProvider.state("app.payments", {
		url: "/payments/{InvoiceID}",
		views: {
			"menuContent": {
				templateUrl: "templates/payments.html",
				controller: "paymentsController"
			}
		}
	});

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise("/splash");
});

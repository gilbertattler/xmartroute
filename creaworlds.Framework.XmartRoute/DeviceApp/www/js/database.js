"use strict";
var appDataBase = angular.module("appDataBase", []);
appDataBase.service("dbService", ["$log", "$q", "daoService", "$http", "notificationService", "cryptService", "$rootScope", "dataService", function ($log, $q, daoService, $http, notificationService, cryptService, $rootScope, dataService) {
	var self = this;

	var dbCommands = [
		"create table if not exists giros (id primary key, name, value, active);",
		"create index if not exists ix_giros_value on giros (value)",

		"create table if not exists frecuencias (id primary key, name, value, active);",
		"create index if not exists ix_frecuencias_value on frecuencias (value);",

		"create table if not exists precioslista (id primary key, name, value, active);",
		"create index if not exists ix_precioslista_value on precioslista (value);",

		"create table if not exists estados (id primary key, name, value, active);",

		"create table if not exists marcas (id primary key, name, value, active);",

		"create table if not exists unidades (id primary key, name, value, active);", //unidades de medida

		"create table if not exists impuestos (id primary key, name, value, formula, active);",

		"create table if not exists categorias (id primary key, parentid, name, value, active);",
		"create index if not exists ix_categorias_value on categorias (value)",
		"create index if not exists ix_categorias_parentid on categorias (parentid)",

		"create table if not exists productos (id primary key, code, name, description, consumptionLevel, unityId, categoryID, parentCategoryID, active);",
		"create index if not exists ix_productoDatos_abc on productos (consumptionLevel)",
		"create index if not exists ix_productoDatos_categorias on productos (categoryID, parentCategoryID)",

		"create table if not exists articulos (id primary key, productid, code, name, barcode, sellunitid, stockunitid, quantity REAL, 'primary', active);",
		"create index if not exists ix_articulos_productid on articulos (productid)",

		"create table if not exists productoImpuesto (productid, taxid, primary key(productid, taxid))",

		"create table if not exists inventario (articleid, lastUpdate, availible, primary key(articleid))",

		"create table if not exists precios (articleid, priceListId, cost, base, taxes, price, profitability, primary key(articleid, priceListId))",

        "create table if not exists clientes (id, parentid, typeId, name, legalName, taxId, address, neighborhood, city, stateId, countryId, zipcode, refers, visitDay, frequencyId, managerName, workPhone, mobilePhone, email, activityID, priceListID, isCreditCustomer, creditLimit, creditUsed, creditAvailible, active, sync, primary key(id))",
        "create table if not exists facturas (id, number, storeid, orderNumber, orderDate, deliveryDate, subTotal, iva, total, pendindPayment, active, deleted, sended, primary key(id))",
		"create table if not exists pagos (id, paymentMethodID, amount, date, reference, comments, active, deleted, primary key(id))",

		/*"drop table ventaDatos", "drop table ventaDetalle", "drop table ventaInfo",*/
		"create table if not exists ventaDatos (id, clienteid, tiendaid, fechar, fechae, total, rentabilidad, sended, activo, borrado, primary key(id))",
		"create table if not exists ventaDetalle (ventaDatosId, articuloId, cantidad, precioBase, precioFinal, total, rentabilidad, activo, borrado, primary key (ventaDatosId, articuloId))",
		"create table if not exists ventaInfo (ventaDatosId, salesComments, financialComments, deliveryComments, primary key(ventaDatosId))",

		"drop view if exists vwVentas",
		"create view if not exists vwVentas AS " +
		"select " +
		"	vd.ID AS ID, strftime('%s', vd.fechar) AS Key, " +
		"	cd.ID AS CustomerID, cd.name AS CustomerAlias, cd.legalName AS CustomerName, cd.taxId AS CustomerTaxID, gd.Name AS CustomerActivity, " +
		"	td.ID AS StoreID,  td.Name AS StoreName, td.ManagerName AS StoreManager, " +
		"	vd.fechar AS CreatedDate, vd.fechae AS SendedDate, vd.Total AS Total, vd.rentabilidad AS Profitability, vd.Sended AS IsSended, vd.Activo AS IsActive " +
		"from ventaDatos AS vd " +
		"inner join ventaInfo AS vi ON vi.ventaDatosId = vd.id " +
		"inner join clientes AS cd on cd.id = vd.clienteid " +
		"inner join clientes as td on td.id = vd.tiendaid " +
		"inner join giros AS gd on gd.id = cd.activityID " +
		"where vd.borrado = 0 " +
		"and cd.active = 'true' " +
		"and td.active = 'true' ",

		"drop view if exists vwRutas ",
		"create view if not exists vwRutas AS " +
		"select " +
		"	td.ID AS ID, " +
		"	td.Name AS Name, " +
		"	td.Address AS Address, " +
		"	td.neighborhood AS Neighborhood, " +
		"	td.City AS CityName, " +
		"	ed.Name AS StateName, " +
		"	td.ZipCode AS ZipCode, " +
		"	td.refers AS [References], " +
		"	td.ManagerName AS ManagerName, " +
		"	td.eMail AS eMail, " +
		"	td.workPhone AS PhoneWork, " +
		"	td.mobilePhone AS PhoneMobile, " +
		"	gd.name AS ActivityName, " +
		"	cd.ID AS CustomerID, " +
		"	cd.Name AS CustomerAlias, " +
		"	cd.legalName AS CustomerName " +
		"	from clientes AS cd " +
		"	inner join clientes AS td on td.parentid = cd.id " +
		"	inner join frecuencias AS fd on fd.id = td.frequencyid " +
		"	inner join estados AS ed ON ed.id = td.stateId " +
		"	inner join giros as gd on gd.id = cd.activityid " +
		"where cast(td.visitDay as integer) = (cast(strftime('%w', date()) as integer) - 1) "
	];

	this.parseFullTextString = function (text) {
		var sqlTerms = [];
		var badWords = ["OR", "AND"];
		// https://www.sqlite.org/cvstrac/wiki?p=FullTextIndex

		text = text.replace(/[*.:+-]+/, ""); //remove restringed chars
		var strTerms = text.split(" ");

		angular.forEach(strTerms, function (value, index) {
			value = value.trim();
			if (value.length >= 3 && badWords.indexOf(value) == -1) {
				sqlTerms.push(value);
			}
		});

		var result = sqlTerms.join("* OR ") + "*";
		//return result.replace(/\s+OR\s+/gi, ""); //quitamos el ultimo 'OR'
		return result;
	};

	var iShowAlert = function (msg, title) {
		notificationService.hideLoading();
		return notificationService.showAlert(msg, title);
	};

	var createSignature = function (token) {
		return daoService.settingsList.apiKey + "|" + daoService.settingsList.apiUserName + "|" + daoService.settingsList.apiPassword + "|" + token;
	};

	var booleanConverter = function (value) {
		return parseInt(value === true ? 1 : 0);
	};

	var floatConverter = function (value) {
		var result = parseFloat(value);
		return (isNaN(result) ? 0 : result);
	}

	var listeners = {
		//listeners as http://stackoverflow.com/questions/18856341/how-can-i-unregister-a-broadcast-event-to-rootscope-in-angularjs
		dbCreated: function () { },
		activitiesLoaded: function () { },
		preciosListaLoaded: function () { },
		statesLoaded: function () { },
		productCategoriesLoaded: function () { },
		productBrandsLoaded: function () { },
		productUnitsLoaded: function () { },
		taxesLoaded: function () { },
		productsLoaded: function () { },
		articlesLoaded: function () { },
		loadFrecuencies: function () { },
		stockLoaded: function () { },

		loadPricesHandler: function () { },

		loadInvoces: function () { },
		dbDone: function () { }

	};

	this.performSync = function () {
		var q = $q.defer();

		//unbind cualquier evento previo
		listeners.dbCreated(); listeners.activitiesLoaded(); listeners.preciosListaLoaded(); listeners.productCategoriesLoaded();
		listeners.productBrandsLoaded(); listeners.productUnitsLoaded(); listeners.taxesLoaded();
		listeners.productsLoaded(); listeners.articlesLoaded();
		listeners.stockLoaded(); listeners.loadFrecuencies();
		listeners.loadPricesHandler();listeners.loadInvoces();
		listeners.dbDone();

		self.performDataBase().then(function () {
			$rootScope.$emit("dbCreated");
		}, function (e) {
			notificationService.hideLoading();
			iShowAlert("Ha ocurrido un error al crear la base de datos.", "DataBase Error").then(function () {
				q.reject(e);
			});
		});

		listeners.dbCreated = $rootScope.$on("dbCreated", function () {
			notificationService.showLoading("Descargando Giros Comerciales");
			self.downloadResource("/Collections/ListActivities").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from giros");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into giros (id, name, value, active) values (?, ?, ?, ?)", [value.ID, value.Name, value.Value, booleanConverter(value.IsActive)]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar los giros comerciales.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("activitiesLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar los giros comerciales.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.dbCreated();
			});
		});

		listeners.activitiesLoaded = $rootScope.$on("activitiesLoaded", function () {
			notificationService.showLoading("Descargando Listas de Precios");
			self.downloadResource("/Collections/ListPriceList").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from precioslista");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into precioslista (id, name, value, active) values (?, ?, ?, ?)", [value.ID, value.Name, value.Value, booleanConverter(value.IsActive)]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar las listas de precios.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("preciosListaLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar las listas de precios.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.activitiesLoaded();
			});
		});

		listeners.preciosListaLoaded = $rootScope.$on("preciosListaLoaded", function () {
			notificationService.showLoading("Descargando Entidades Federativas");
			self.downloadResource("/Collections/ListStates").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from estados");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into estados (id, name, value, active) values (?, ?, ?, ?)", [value.ID, value.Name, value.Value, booleanConverter(value.IsActive)]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar las entidades federativas.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("statesLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar las entidades federativas.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.preciosListaLoaded();
			});
		});

		listeners.statesLoaded = $rootScope.$on("statesLoaded", function () {
			notificationService.showLoading("Descargando Grupos de Articulos");
			self.downloadResource("/Products/CategoriesList").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from categorias");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into categorias (id, parentid, name, value, active) values (?, ?, ?, ?, ?)", [value.ID, value.Tag, value.Name, value.Value, booleanConverter(value.IsActive)]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar los grupos de articulos.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("productCategoriesLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar los grupos de articulos.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.statesLoaded();
			});
		});

		listeners.productCategoriesLoaded = $rootScope.$on("productCategoriesLoaded", function () {
			notificationService.showLoading("Descargando Marcas de Producto");
			self.downloadResource("/Products/BrandsList").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from marcas");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into marcas (id, name, value, active) values (?, ?, ?, ?)", [value.ID, value.Name, value.Value, booleanConverter(value.IsActive)]);
					});;
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar el catalogo de marcas de articulos.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("productBrandsLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar el catalogo de marcas de articulos.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.productCategoriesLoaded();
			});
		});

		listeners.productBrandsLoaded = $rootScope.$on("productBrandsLoaded", function () {
			notificationService.showLoading("Descargando Unidades de Medida");
			self.downloadResource("/Products/UnitsList").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from unidades");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into unidades (id, name, value, active) values (?, ?, ?, ?)", [value.ID, value.Name, value.Value, booleanConverter(value.IsActive)]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar el catalogo de unidades de medida.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("productUnitsLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar el catalogo de unidades de medida.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.productBrandsLoaded();
			});
		});

		listeners.productUnitsLoaded = $rootScope.$on("productUnitsLoaded", function () {
			notificationService.showLoading("Descargando Catalogo de Impuestos");
			self.downloadResource("/Collections/ListTaxes").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from impuestos");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into impuestos (id, name, value, formula, active) values (?, ?, ?, ?, ?)", [value.ID, value.Name, value.Value, floatConverter(value.Tag), booleanConverter(value.IsActive)]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar el catalogo de impuestos.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("taxesLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar el catalogo de impuestos.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.productUnitsLoaded();
			});
		});

		listeners.taxesLoaded = $rootScope.$on("taxesLoaded", function () {
			notificationService.showLoading("Descargando Catalogo de Productos");
			self.downloadResource("/Products/List").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from productos");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into productos (id, code, name, description, consumptionLevel, unityId, categoryID, parentCategoryID, active) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", [
							value.ID, value.Code,
							value.Name, value.Description,
							value.ConsumptionLevel, value.UnitID, value.CategoryID, value.ParentCategoryID,
							booleanConverter(value.IsActive)]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar el catalogo de productos.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("productsLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar el catalogo de productos.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.taxesLoaded();
			});
		});

		listeners.productsLoaded = $rootScope.$on("productsLoaded", function () {
			notificationService.showLoading("Descargando Catalogo de Articulos");
			self.downloadResource("/Products/ArticleList").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from articulos");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into articulos (id, productid, code, name, barcode, sellunitid, stockunitid, quantity, 'primary', active) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
							value.ID, value.ProductID, value.Code,
							value.Name, value.BarCode,
							value.SellUnitID, value.StockUnitID,
							floatConverter(value.Quantity),
							booleanConverter(value.IsPrimary),
							booleanConverter(value.IsActive)
						]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar el catalogo de articulos.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("articlesLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar el catalogo de articulos.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.productsLoaded();
			});
		});

		listeners.articlesLoaded = $rootScope.$on("articlesLoaded", function () {
			notificationService.showLoading("Descargando Impuestos de Productos");
			self.downloadResource("/Products/TaxesList").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from productoImpuesto");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into productoImpuesto (productid, taxid) values (?, ?)", [value.ProductID, value.TaxID]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar los impuestos por articulo.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("stockLoaded");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar los impuestos por articulo.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.articlesLoaded();
			});
		});

		listeners.stockLoaded = $rootScope.$on("stockLoaded", function () {
			notificationService.showLoading("Descargando Inventarios");
			self.downloadResource("/Products/DownloadStock").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from inventario ");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into inventario (articleid, lastUpdate, availible) values (?, ?, ?)", [value.ArticleID, value.UpdatedDate, value.Availible]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar las existencias de un articulo.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("loadSellPrices");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar el inventario.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.stockLoaded();
			});
		});

		listeners.loadPricesHandler = $rootScope.$on("loadSellPrices", function () {
			notificationService.showLoading("Descargando Precios de Art&iacute;culos");
			self.downloadResource("/Products/DownloadSellPrices").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					if (r.CodeNumber == 302) {
						tx.executeSql("delete from precios");
						angular.forEach(r.Result, function (value, key) {
							tx.executeSql("insert into precios (articleid, priceListId, cost, base, taxes, price, profitability) values (?, ?, ?, ?, ?, ?, ?)", [
								value.ArticleID, value.PriceListID,
								value.Cost, value.Base, value.Taxes, value.Price, value.Profitability
							]);
						});
					}
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar los precios de los artículos.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("loadFrecuencies");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar las precios de los artículos.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.loadPricesHandler();
			});
		});

		listeners.loadFrecuencies = $rootScope.$on("loadFrecuencies", function () {
			notificationService.showLoading("Descargando Frecuencias de Visita");
			self.downloadResource("/Collections/ListFrequencies").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from frecuencias");
					angular.forEach(r.Result, function (value, key) {
						tx.executeSql("insert into frecuencias (id, name, value, active) values (?, ?, ?, ?)", [value.ID, value.Name, value.Value, booleanConverter(value.IsActive)]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar las frecuencias de visita.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("loadInvoces");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar las frecuencias de visita.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.loadFrecuencies();
			});
		});

		listeners.loadInvoces = $rootScope.$on("loadInvoces", function () {
			notificationService.showLoading("Descargando Facturas");
			self.downloadResource("/Collections/ListInvoces").then(function (r) {
				self.openDatabase().transaction(function (tx) {
					tx.executeSql("delete from facturas");
					angular.forEach(r.Result, function (value, key) {

						tx.executeSql("insert into facturas (id, number, storeid, orderNumber, orderDate, deliveryDate, subTotal, iva, total, pendindPayment, active, deleted, sended) "
									  "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
							[value.ID, value.Number, value.StoreID, value.OrderNumber, value.OrderDate, value.DeliveryDate, value.SubTotal, value.IVA, value.Total, value.PendindPayment, booleanConverter(value.IsActive), value.Deleted,value.Sended ]);
					});
				}, function (e) {
					//onerror
					$log.error(e);
					notificationService.hideLoading();
					notificationService.showAlert("Ha ocurrido un error al almacenar las facturas.", "Error").then(function () {
						q.reject(e);
					});
				}, function () {
					//onsuccess
					$rootScope.$emit("dbDone");
				});
			}, function (e) {
				$log.error(e);
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error de comunicación al descargar las facturas.", "Error").then(function () {
					q.reject(e);
				});
			}).then(function () {
				listeners.loadInvoces();
			});
		});


		listeners.dbDone = $rootScope.$on("dbDone", function () {
			self.loadData().then(function () {
				q.resolve();
			}, function (e) {
				q.reject(e);
			}).then(function () {
				listeners.dbDone();
				notificationService.hideLoading();
			});
		});

		return q.promise;
	};

	// Returns a ready database object
	this.openDatabase = function () {
		//cargamos los valores de configuración y parseamos el tamaño de la db
		var iSize = parseInt(daoService.loadSettings().settingsList.dbSize) * 1048576;
		return window.openDatabase(daoService.settingsList.dbName, daoService.settingsList.dbVersion, daoService.settingsList.dbAlias, daoService.settingsList.iSize);
	};

	// perform the sql commands to init the app
	this.performDataBase = function () {
		var q = $q.defer();
		var db = self.openDatabase();

		notificationService.showLoading("Creando Base de Datos");
		db.transaction(function (tx) {
			//iterate every initial sql command 
			angular.forEach(dbCommands, function (value, key) {
				if (value.trim() !== "") {
					tx.executeSql(value);
				}
			});

			//creamos la base de datos de clientes con soporte de fulltext
			tx.executeSql("select DISTINCT tbl_name from sqlite_master where tbl_name = 'clientes'", [], function (tx, rs) {
				if (rs.rows.length == 0) {
					//tx.executeSql("CREATE VIRTUAL TABLE clientes USING FTS3 (id, parentid, typeId, name, legalName, taxId, address, neighborhood, city, stateId, countryId, zipcode, refers, visitDay, frequencyId, managerName, workPhone, mobilePhone, email, activityID, priceListID, isCreditCustomer, creditLimit, creditUsed, creditAvailible, active, sync)");
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject(e);
		}, function () {
			//onsuccess
			q.resolve();
		});

		return q.promise;
	};

	this.downloadResource = function (url) {
		var q = $q.defer();

		var time = new Date().valueOf();
		var iUrl = daoService.settingsList.apiUrl + url;
		var sign = createSignature(time);

		//encriptamos la firma y procedemos con la solicitud $http
		var md5 = cryptService.md5(sign).then(function (hash) {
			var api = $http.post(iUrl, {
				UserName: daoService.settingsList.apiUserName,
				Password: daoService.settingsList.apiPassword,
				Token: time,
				Signature: hash
			});

			api.success(function (r) {
				if (r.CodeNumber == 302 || r.CodeNumber == 404) {
					q.resolve(r);
				} else {
					q.reject(r);
				}
			});

			api.error(function (e) {
				$log.error(e);
				q.reject({ CodeNumber: 504, Message: e });
			});

			api.finally(function () {

			});
		});

		return q.promise;
	};	

	this.loadData = function () {
		var q = $q.defer();

		notificationService.showLoading("Conectando a Base de Datos Local");
		self.performDataBase().then(function () {
			self.openDatabase().transaction(function (tx) {
				notificationService.showLoading("Cargando Giros");
				tx.executeSql("select * from giros as g where g.active = 1 and g.value != ? order by g.name", ["00"], function (tx, rs) {
					dataService.businessLineList = [];
					for (var i = 0; i < rs.rows.length; i++) {
						var row = rs.rows.item(i);
						dataService.businessLineList.push({ ID: row.id, Name: row.name, IsActive: row.active });
					}
				});

				notificationService.showLoading("Cargando Listas de Precios");
				tx.executeSql("select * from precioslista as g where g.active = 1 and g.value != ? order by g.name", ["00"], function (tx, rs) {
					dataService.priceList = [];
					for (var i = 0; i < rs.rows.length; i++) {
						var row = rs.rows.item(i);
						dataService.priceList.push({ ID: row.id, Name: row.name, IsActive: row.active });
					}
				});

				notificationService.showLoading("Cargando Entidades Federativas");
				tx.executeSql("select * from estados as g where g.active = 1 order by g.name", [], function (tx, rs) {
					dataService.statesList = [];
					for (var i = 0; i < rs.rows.length; i++) {
						var row = rs.rows.item(i);
						dataService.statesList.push({ ID: row.id, Name: row.name, IsActive: row.active });
					}
				});

				notificationService.showLoading("Cargando Grupos de Articulos");
				tx.executeSql("select * from categorias as g where g.active = 1", [], function (tx, rs) {
					dataService.categoriesList = [];
					for (var i = 0; i < rs.rows.length; i++) {
						var row = rs.rows.item(i);
						dataService.categoriesList.push({ ID: row.id, Name: row.name, IsActive: row.active });
					}
				});

				notificationService.showLoading("Cargando Frecuencias de Visita");
				tx.executeSql("select * from frecuencias as f where f.active = 1", [], function (tx, rs) {
					dataService.frequenciesList = [];
					for (var i = 0; i < rs.rows.length; i++) {
						var row = rs.rows.item(i);
						dataService.frequenciesList.push({ ID: row.id, Name: row.name, IsActive: row.active });
					}
				});

				/************************************************************************
				*	SE INHIBE DEBIDO A QUE NO ES NECESARIO CARGARLOS EN UN CATALOGO		*
				*************************************************************************
				notificationService.showLoading("Cargando Marcas de Articulos");
				tx.executeSql("select * from marcas as g where g.active = 1", [], function (tx, rs) {
					dataService.categoriesList = [];
					for (var i = 0; i < rs.rows.length; i++) {
						var row = rs.rows.item(i);
						dataService.brandsList.push({ ID: row.id, Name: row.name, IsActive: row.active });
					}
				});
				*/
			}, function (e) {
				//error
				$log.error(e);
				q.reject(e);
			}, function () {
				//success
				q.resolve();
			});
		}, function (e) {
			q.reject(e);
		});

		return q.promise;
	};

	this.getSellsNotSync = function () {
		var q = $q.defer();

		var sells = [];
		var details = [];

		$log.info("Conectando a la Base de Datos Local");
		self.openDatabase().transaction(function (tx) {
			$log.info("consultando ventas.");

			tx.executeSql("SELECT d.*,i.* FROM ventaDatos AS d " +
						  "INNER JOIN ventaInfo AS i ON d.id=i.ventaDatosId " + 
						  "WHERE activo = 1 AND borrado = 0 AND sended = 0", [], 

			function (txSell, rsSell) {
				for (var i = 0; i < rsSell.rows.length; i++) {
					var rowSell = rsSell.rows.item(i);
					$log.info("consultando los detalles de la venta.");
					tx.executeSql("SELECT * FROM ventaDetalle " +
								  "WHERE ventaDatosId = ? AND activo = 1 AND borrado = 0", [rowSell.id], 
					function (txDetail, rsDetail) {
						details = [];
						for (var i = 0; i < rsDetail.rows.length; i++) {
							var rowDetail = rsDetail.rows.item(i);
							details.push({ 
								ID: rowDetail.id, 
								RegularPrice: rowDetail.precioBase, 
								ArticleID: rowDetail.articuloId, 
								Amount: rowDetail.cantidad, 
								FinalPrice: rowDetail.precioFinal, 
								Total: rowDetail.total, 
								Profitability: rowDetail.rentabilidad});
						}
					});

					sells.push({
						ID:rowSell.id,
						Items:details,
						CustomerID:rowSell.clienteid,
						StoreID:rowSell.tiendaid,
						CreatedDate:rowSell.fechar,
						Total:rowSell.total,
						Profitability:rowSell.rentabilidad,
						SellsComments:rowSell.salesComments,
						DeliveryComments:rowSell.deliveryComments,
						FinancialComments:rowSell.financialComments});
				}
			});
		}, function (e) {
			$log.error(e);
			q.reject(e);
		}, function () {
			$log.info('Consulta de ventas éxitosa');
			q.resolve(sells);
		});

		return q.promise;
	};

	this.getCustomersNotSync = function () {
		var q = $q.defer();

		var customers = [];

		$log.info("Conectando a la Base de Datos Local");
		self.openDatabase().transaction(function (tx) {
			$log.info("consultando clientes.");
			tx.executeSql("SELECT * FROM clientes WHERE active = 'true' AND parentid IS NULL AND sync = 0", [], 
			function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					customers.push({
						ID: row.id,
						TypeID: row.typeId,
						Name: row.name,
						LegalName: row.legalName,
						TaxID: row.taxId,
						Address: row.address,
						Neighborhood: row.neighborhood,
						CityName: row.city,
						StateName: row.stateId,
						CountryName: row.countryId,
						ZipCode: row.zipcode,
						ManagerName: row.managerName,
						PhoneWork: row.workPhone,
						PhoneMobile: row.mobilePhone,
						eMail: row.email,
						IsCreditCustomer: row.isCreditCustomer,
						CreditLimit: row.creditLimit,
						CreditUsed: row.creditUsed,
						CreditAvailible: row.creditAvailible,
						ActivityID: row.active 
					});
				}
			});
		}, function (e) {
			$log.error(e);
			q.reject(e);
		}, function () {
			$log.info('Consulta de clientes éxitosa');
			q.resolve(customers);
		});

		return q.promise;
	};

	this.getStoresNotSync = function () {
		var q = $q.defer();

		var stores = [];

		$log.info("Conectando a la Base de Datos Local");
		self.openDatabase().transaction(function (tx) {
			$log.info("consultando tiendas.");
			tx.executeSql("SELECT * FROM clientes WHERE active = 'true' AND parentid IS NOT NULL AND sync = 0", [], 
			function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					stores.push({
					ID: row.id,
					ParentID: row.parentid,
					TypeID: row.typeId,
					Name: row.name,
					Address: row.address,
					Neighborhood: row.neighborhood,
					CityName: row.city,
					StateName: row.stateId,
					CountryName: row.countryId,
					ZipCode: row.zipcode,
					Refers: row.refers,
					VisitingDay: row.visitDay,
					FrequencyId: row.frequencyId,
					ManagerName: row.managerName,
					PhoneWork: row.workPhone,
					PhoneMobile: row.mobilePhone,
					eMail: row.email
					});
				}
			});
		}, function (e) {
			$log.error(e);
			q.reject(e);
		}, function () {
			$log.info('Consulta de tiendas éxitosa');
			q.resolve(stores);
		});

		return q.promise;
	};
}]);
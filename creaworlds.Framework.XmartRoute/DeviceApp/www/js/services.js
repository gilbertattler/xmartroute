﻿var appServices = angular.module("appServices", []);

appServices.service("daoService", ["$log", "$q", function ($log, $q) {
	var self = this;
	var settingsLoaded = false;

	this.settingsList = {}; //private settings list

	this.defaultSettings = {
		dbName: "xmartRoute",
		dbVersion: "1",
		dbAlias: "xmartRoute database",
		dbSize: "5",
		apiDelay: "5"
	};

	// Load the settings saved on local storage
	this.loadSettings = function () {
		if (!settingsLoaded) {
			self.settingsList.dbName = self.getSettingsValue("dbName", self.defaultSettings.dbName);
			self.settingsList.dbVersion = self.getSettingsValue("dbVersion", self.defaultSettings.dbVersion);
			self.settingsList.dbAlias = self.getSettingsValue("dbAlias", self.defaultSettings.dbAlias);
			self.settingsList.dbSize = self.getSettingsValue("dbSize", self.defaultSettings.dbSize); //size in MB
			//settingsList.dbCommands = self.getSettingsValue("dbCommands").split(";");

			self.settingsList.apiKey = self.getSettingsValue("apiKey");
			self.settingsList.apiUserName = self.getSettingsValue("apiUserName");
			self.settingsList.apiPassword = self.getSettingsValue("apiPassword");
			self.settingsList.apiUrl = self.getSettingsValue("apiUrl");
			self.settingsList.apiDelay = parseInt(self.getSettingsValue("apiDelay", self.defaultSettings.apiDelay)); // time in minutes

			self.settingsList.appSplash = self.getSettingsValue("appSplash", "3"); // time in seconds
			settingsLoaded = true;
		}

		return self;
	};

	// Read an specified setting item on local storage
	this.getSettingsValue = function (name, defaultValue) {
		var result = "";
		try {
			var tmp = window.localStorage.getItem(name);
			if (tmp == null) {
				throw "Not Found Keypair on local storage.";
			} else {
				result = tmp;
			}
		} catch (e) {
			$log.info("Unable to locate a keypair value from settings storage.");

			if (angular.isDefined(defaultValue)) {
				result = defaultValue;
			}
		}
		return result;
	};

	// Save an specified setting item on local storage
	this.setSettingsValue = function (name, value) {
		try {
			window.localStorage.setItem(name, value);
		} catch (e) {
			$log.error("Unable to save a keypair value to settings storage.");
			throw e;
		}
	};
}]);

appServices.service("dataService", ["$log", "$q", function ($log, $q) {
	var self = this;

	this.priceList = [];
	this.businessLineList = [];
	this.statesList = [];
	this.categoriesList = [];
	this.brandsList = [];

	this.frequenciesList = [];

	this.dayList = [
		{ ID: "00", Name: "Domingo", Value: "00", IsActive: true },
		{ ID: "01", Name: "Lunes", Value: "01", IsActive: true },
		{ ID: "02", Name: "Martes", Value: "02", IsActive: true },
		{ ID: "03", Name: "Miercoles", Value: "03", IsActive: true },
		{ ID: "04", Name: "Jueves", Value: "04", IsActive: true },
		{ ID: "05", Name: "Viernes", Value: "05", IsActive: true },
		{ ID: "06", Name: "Sábado", Value: "06", IsActive: true },
	];
	this.paymentMethods = [
		{ID: "00",Name: "Efectivo", Value:"00", IsActive: true},
		{ID: "01",Name: "Cheque", Value:"01", IsActive: true},
		{ID: "02",Name: "Transferencia", Value:"02", IsActive: true}
	];
}]);

appServices.service("cryptService", ["$log", "$q", "$window", function ($log, $q, $window) {
	this.md5 = function (text) {
		var q = $q.defer();

		window.hashString({ data: text, hash: "md5" }, function (hash) {
			q.resolve(hash);
		});

		return q.promise;
	};

	this.uuid = function () {
		var q = $q.defer();

		window.hashString({ data: "", hash: "uuid" }, function (hash) {
			q.resolve(hash);
		});

		return q.promise;
	};
}]);

appServices.service("notificationService", ["$log", "$q", "$ionicPopup", "$ionicLoading", function ($log, $q, $ionicPopup, $ionicLoading) {
	this.showConfirm = function (msg, title) {
		// return a promise
		return $ionicPopup.confirm({
			title: (angular.isDefined(title) ? title : 'Favor de Confirmar'),
			template: '<p style="text-align:center;">&iquest;' + msg + '&#63;</p>'
		});
	};

	this.showAlert = function (msg, title) {
		return $ionicPopup.alert({
			title: (angular.isDefined(title) ? title : 'Informaci&oacute;n'),
			template: '<p style="text-align:center;">' + msg + '</p>'
		});
	};

	this.showLoading = function (text) {
		$ionicLoading.show({ template: text });
	};

	this.hideLoading = function () {
		$ionicLoading.hide();
	};
}]);

appServices.service("splashService", ["$log", "$q", "daoService", "dbService", "$timeout", function ($log, $q, daoService, dbService, $timeout) {
	this.load = function () {
		var q = $q.defer();

		dbService.loadData().then(function () {
			var delay = parseInt(daoService.settingsList.appSplash) * 1000;
			$timeout(function () {
				q.resolve();
			}, delay);
		}, function (e) {
			q.reject(e);
		});

		return q.promise;
	};
}]);

appServices.service("settingsService", ["$log", "$q", "daoService", "dbService", function ($log, $q, daoService, dbService) {
	var self = this;
	this.getSettings = function () {
		return daoService.settingsList;
	};

	this.clearSettings = function () {
		daoService.setSettingsValue("apiKey", "");
		daoService.setSettingsValue("apiUserName", "");
		daoService.setSettingsValue("apiPassword", "");
		daoService.setSettingsValue("apiUrl", "");
		daoService.setSettingsValue("apiDelay", daoService.defaultSettings.apiDelay);
		daoService.loadSettings();
	};

	this.saveSettings = function (data) {
		daoService.setSettingsValue("apiKey", data.apiKey.toUpperCase());
		daoService.setSettingsValue("apiUserName", data.apiUserName.toLowerCase());
		daoService.setSettingsValue("apiPassword", data.apiPassword);
		daoService.setSettingsValue("apiUrl", data.apiUrl);
		daoService.setSettingsValue("apiDelay", data.apiDelay);
		daoService.loadSettings();
	};

	this.dbSynchronize = function () {
		//dbService.performSync() returns a promise
		return dbService.performSync();
	};
}]);

appServices.service("customerSearchService", ["$log", "$q", "dbService", function ($log, $q, dbService) {
	var self = this;

	this.search = function (text) {
		var q = $q.defer();

		var customersList = [];
		dbService.openDatabase().transaction(function (tx) {
			var sql = "select clientes.*, estados.name as stateName " +
					  "from clientes " +
					  "left join estados on estados.id = clientes.stateId " +
					  "where (clientes.name like ? or clientes.legalName like ?) " +
					  "and clientes.parentid is null " +
					  "and clientes.active = 'true' " +
					  "and clientes.active = 'true' ";
		    //var str = dbService.parseFullTextString(text);
			var str = "%" + text + "%";
			tx.executeSql(sql, [str, str], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					customersList.push({
						ID: row.id,
						Name: row.legalName, Alias: row.name, TaxID: row.taxId,
						Address: row.address, Neighborhood: row.neighborhood, CityName: row.city, StateID: row.stateId, StateName: row.stateName, ZipCode: row.zipcode,
						ManagerName: row.managerName, eMail: row.email, WorkPhone: row.workPhone, MobilePhone: row.mobilePhone,
						PriceListID: row.priceListID, BusinessLineID: row.activityID
					});
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al realizar la b&uacute;squeda de un cliente." });
		}, function () {
			//onsuccess
			q.resolve(customersList);
		});

		return q.promise;
	};

	this.list = function () {
		var q = $q.defer();

		var customersList = [];
		dbService.openDatabase().transaction(function (tx) {
			var sql = "select clientes.*, estados.name as stateName " +
					  "from clientes " +
					  "left join estados on estados.id = clientes.stateId " +
					  "where clientes.parentid is null " +
					  "and clientes.active = 'true' " +
					  "and clientes.active = 'true' ";
			tx.executeSql(sql, [], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					customersList.push({
						ID: row.id,
						Name: row.legalName, Alias: row.name, TaxID: row.taxId,
						Address: row.address, Neighborhood: row.neighborhood, CityName: row.city, StateID: row.stateId, StateName: row.stateName, ZipCode: row.zipcode,
						ManagerName: row.managerName, eMail: row.email, WorkPhone: row.workPhone, MobilePhone: row.mobilePhone,
						PriceListID: row.priceListID, BusinessLineID: row.activityID
					});
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al realizar el listado de clientes." });
		}, function () {
			//onsuccess
			q.resolve(customersList);
		});

		return q.promise;
	};
}]);

appServices.service("customersService", ["$log", "$q", "dbService", "cryptService", function ($log, $q, dbService, cryptService) {
	this.save = function (data) {
		var q = $q.defer();

		cryptService.uuid().then(function (uuid) {
			dbService.openDatabase().transaction(function (tx) {
				data.Alias = data.Alias.toUpperCase();
				data.Name = data.Name.toUpperCase();
				data.TaxID = data.TaxID.toUpperCase();
				data.Address = data.Address.toUpperCase();
				data.Neighborhood = data.Neighborhood.toUpperCase();
				data.CityName = data.CityName.toUpperCase();
				data.ManagerName = data.ManagerName.toUpperCase();
				data.eMail = data.eMail.toLowerCase();

				if (data.ID == "") {
					var sql = "insert into clientes (id, typeId, name, legalName, taxId, address, neighborhood, city, stateId, countryId, zipcode, managerName, workPhone, mobilePhone, email, activityID, priceListID, isCreditCustomer, creditLimit, creditUsed, creditAvailible, active, sync) " +
							  "values (?, 'C', ?, ?, ?, ?, ?, ?, ?, 'MX', ?, ?, ?, ?, ?, ?, ?, 0, 0, 0, 0, 'true', 0)";
					tx.executeSql(sql, [uuid, data.Alias, data.Name, data.TaxID, data.Address, data.Neighborhood, data.CityName, data.StateID, data.ZipCode, data.ManagerName, data.WorkPhone, data.MobilePhone, data.eMail, data.BusinessLineID, data.PriceListID]);
					data.ID = uuid;
				} else {
					var sql = "update clientes " +
							  "set name = ?, legalName = ?, taxId = ?, " +
							  "    address = ?, neighborhood = ?, city = ?, stateId = ?, zipcode = ?, " +
							  "    managerName = ?, workPhone = ?, mobilePhone = ?, email = ?, " +
							  "    activityID = ?, priceListID = ?, " +
							  "    sync = 0 " +
							  "where id = cast(? as text)";
					tx.executeSql(sql, [
						data.Alias, data.Name, data.TaxID,
						data.Address, data.Neighborhood, data.CityName, data.StateID, data.ZipCode,
						data.ManagerName, data.WorkPhone, data.MobilePhone, data.eMail,
						data.BusinessLineID, data.PriceListID,
						data.ID
					], function (tx, rs) {
						if (rs.rowsAffected == 0) {
							throw "No se han afectado filas al actualizar un cliente.";
						}
					});
				}
			}, function (e) {
				//onerror
				$log.error(e);
				q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al actualizar los datos del cliente." });
			}, function () {
				//onsuccess
				q.resolve();
			});
		});

		return q.promise;
	};

	this.find = function (id) {
		var q = $q.defer();
		var result = null;

		dbService.openDatabase().transaction(function (tx) {
			tx.executeSql("select c.* from clientes AS c where c.id = ? and c.parentid is null and c.active = 'true' ", [id], function (tx, rs) {
				if (rs.rows.length == 1) {
					var row = rs.rows.item(0);
					result = {
						ID: row.id,
						Name: row.legalName, Alias: row.name, TaxID: row.taxId,
						Address: row.address, Neighborhood: row.neighborhood, CityName: row.city, StateID: row.stateId, ZipCode: row.zipcode,
						ManagerName: row.managerName, eMail: row.email, WorkPhone: row.workPhone, MobilePhone: row.mobilePhone,
						PriceListID: row.priceListID, BusinessLineID: row.activityID
					};
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error obtener los datos del cliente." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};

}]);

appServices.service("storesService", ["$log", "$q", "dbService", "cryptService", function ($log, $q, dbService, cryptService) {
	this.list = function (customerId) {
		var q = $q.defer();
		var result = [];

		dbService.openDatabase().transaction(function (tx) {
			tx.executeSql("select c.* from clientes AS c where c.parentid = ? and c.active = 'true' order by c.name", [customerId], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					result.push({
						ID: row.id, ParentID: row.parentid,
						Name: row.legalName, Alias: row.name, TaxID: row.taxId,
						Address: row.address, Neighborhood: row.neighborhood, CityName: row.city, StateID: row.stateId, ZipCode: row.zipcode,
						ManagerName: row.managerName, eMail: row.email, WorkPhone: row.workPhone, MobilePhone: row.mobilePhone,
						PriceListID: row.priceListID, BusinessLineID: row.activityID
					});
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error obtener los datos del cliente." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};

	this.find = function (id) {
		var q = $q.defer();
		var result = null;

		dbService.openDatabase().transaction(function (tx) {
			tx.executeSql("select c.* from clientes AS c where c.id = ? and c.parentid is not null", [id], function (tx, rs) {
				if (rs.rows.length == 1) {
					var row = rs.rows.item(0);
					result = {
						ID: row.id,
						Name: row.name,
						Address: row.address, Neighborhood: row.neighborhood, CityName: row.city, StateID: row.stateId, ZipCode: row.zipcode, References: row.refers, WeekDay: row.visitDay, FrequencyID: row.frequencyId,
						ManagerName: row.managerName, eMail: row.email, WorkPhone: row.workPhone, MobilePhone: row.mobilePhone,
						ParentID: row.parentid
					};
				} else {
					throw "Tienda no encontrada.";
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error obtener los datos del cliente." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};

	this.save = function (data) {
		var q = $q.defer();

		cryptService.uuid().then(function (uuid) {
			dbService.openDatabase().transaction(function (tx) {
				data.Name = data.Name.toUpperCase();
				data.Address = data.Address.toUpperCase();
				data.Neighborhood = data.Neighborhood.toUpperCase();
				data.CityName = data.CityName.toUpperCase();
				data.References = data.References.toUpperCase();
				data.ManagerName = data.ManagerName.toUpperCase();
				data.eMail = data.eMail.toLowerCase();

				if (data.ID == "") {
					var sql = "insert into clientes (" +
							  "		id, typeId, parentid, " +
							  "		name, " +
							  "		address, neighborhood, city, stateId, countryId, zipcode, refers, visitDay, frequencyId, " +
							  "		managerName, workPhone, mobilePhone, email, " +
							  "		active, sync) " +
							  "values (?, 'S', ?, ?, ?, ?, ?, ?, 'MX', ?, ?, ?, ?, ?, ?, ?, ?, 'true', 0)";
					tx.executeSql(sql, [
						uuid, data.ParentID, data.Name,
						data.Address, data.Neighborhood, data.CityName, data.StateID, data.ZipCode, data.References, data.WeekDay, data.FrequencyID,
						data.ManagerName, data.WorkPhone, data.MobilePhone, data.eMail
					]);
					data.ID = uuid;
				} else {
					var sql = "update clientes " +
							  "set name = ?, " +
							  "    address = ?, neighborhood = ?, city = ?, stateId = ?, zipcode = ?, refers = ?, visitDay = ?, frequencyId = ?," +
							  "    managerName = ?, workPhone = ?, mobilePhone = ?, email = ?, " +
							  "    sync = 0 " +
							  "where id = ? " +
							  "and parentid = ? ";
					tx.executeSql(sql, [
						data.Name,
						data.Address, data.Neighborhood, data.CityName, data.StateID, data.ZipCode, data.References, data.WeekDay, data.FrequencyID,
						data.ManagerName, data.WorkPhone, data.MobilePhone, data.eMail, data.ID, data.ParentID], function (tx, rs) {
							if (rs.rowsAffected == 0) {
								throw "No se han afectado filas al actualizar una tienda";
							}
						});
				}
			}, function (e) {
				//onerror
				$log.error(e);
				q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al actualizar los datos de la sucursal." });
			}, function () {
				//onsuccess
				q.resolve();
			});
		});


		return q.promise;
	};
	
}]);

appServices.service("productsService", ["$log", "$q", "dbService", function ($log, $q, dbService) {
	this.search = function (customerId, text) {
		var q = $q.defer();
		var result = [];

		dbService.openDatabase().transaction(function (tx) {
			//id, code, name, description, consumptionLevel, unityId, categoryID, parentCategoryID, active
			var sql = "select " +
					"	a.*, " +
					"	p.code AS productCode, " +
					"	u.name AS unitName, " +
					"	c.name AS categoryName, " +
					"	i.availible AS availibleStock , " +
					"	x.cost as cost, " +
					"	x.base as basePrice, " +
					"	x.taxes as taxes, " +
					"	x.price as defaultPrice, " +
					"	x.profitability AS profitability " +
					"from articulos AS a  " +
					"join productos AS p on p.id = a.productid  " +
					"join unidades AS u on u.id = a.sellunitid  " +
					"join categorias AS c on c.id = p.categoryID  " +
					"join inventario as i on i.articleid = a.id  " +
					"join (" +
					"	select p.* " +
					"	from clientes AS c " +
					"	join precios AS p on p.priceListID = c.priceListID " +
					"	where c.id = ? " +
					") AS x on x.articleid = a.id " +
					"where (a.code like ? OR a.name like ?) " +
					"and a.active = 1 " +
					"and p.active = 1 " +
					"order by a.name asc";
			var searchText = "%" + text + "%";
			tx.executeSql(sql, [customerId, searchText, searchText], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					result.push({
						ID: row.id,
						Code: row.code,
						Name: row.name,
						ParentID: row.productid,
						ParentCode: row.productCode,
						BarCode: row.barcode,
						CategoryName: row.categoryName,
						UnitName: row.unitName,
						Stock: row.availibleStock,
						Cost: row.cost,
						DefaultPrice: row.defaultPrice,
						Profitability: row.profitability,
						Amount: 0
					});
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al buscar el producto." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};
}]);

appServices.service("ordersService", ["$log", "$q", "dbService", "cryptService", function ($log, $q, dbService, cryptService) {
	this.saveOrder = function (data) {
		var q = $q.defer();

		cryptService.uuid().then(function (uuid) {
			dbService.openDatabase().transaction(function (tx) {
				var sql = "";
				sql = "insert into ventaDatos (id, clienteid, tiendaid, fechar, total, rentabilidad, sended, activo, borrado) " +
					"values (?,?,?,datetime(),?,?,0,1,0)";
				tx.executeSql(sql, [uuid, data.CustomerID, data.StoreID, data.Total, data.Profitability]);

				sql = "insert into ventaInfo (ventaDatosId, salesComments, financialComments, deliveryComments) values (?, ?, ?, ?)";
				tx.executeSql(sql, [uuid, data.SalesComments, data.FinancialComments, data.DeliveryComments]);

				angular.forEach(data.Items, function (item, index) {
					sql = "insert into ventaDetalle (ventaDatosId, articuloId, cantidad, precioBase, precioFinal, total, rentabilidad, activo, borrado) " +
						"values (?,?,?,?,?,?,?, 1, 0)";
					tx.executeSql(sql, [
						uuid,
						item.ID,
						item.Amount,
						item.DefaultPrice,
						item.CurrentPrice,
						(item.CurrentPrice * item.Amount).toFixed(4),
						item.Profitability
					]);
				});
			}, function (e) {
				//onerror
				$log.error(e);
				q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al guardar la venta." });
			}, function () {
				//onsuccess
				q.resolve(uuid);
			});
		});

		return q.promise;
	};

	this.find = function (id) {
		var q = $q.defer();
		var result = {};

		dbService.openDatabase().transaction(function (tx) {
			var sql = "";
			sql = "select *, strftime('%s', vd.fechar) AS folio " +
				"from ventaDatos AS vd " +
				"inner join ventaInfo AS vi ON vi.ventaDatosID = vd.ID " +
				"where vd.id = ? " +
				"and vd.activo = 1 " +
				"and vd.borrado = 0 ";
			tx.executeSql(sql, [id], function (tx, rs) {
				if (rs.rows.length == 1) {
					var row = rs.rows.item(0);
					result = {
						ID: row.id,
						Key: row.folio,
						CustomerID: row.clienteid,
						StoreID: row.tiendaid,
						CreatedDate: row.fechar,
						SendedDate: row.fechae,
						Total: row.total,
						Profitability: row.rentabilidad,
						SalesComments: row.salesComments,
						FinancialComments: row.financialComments,
						DeliveryComments: row.deliveryComments,
						IsSended: (row.sended == 1),
						IsActive: (row.activo == 1)
					};
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al localizar una venta." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};

	this.getItems = function (id) {
		var q = $q.defer();
		var result = [];

		dbService.openDatabase().transaction(function (tx) {
			var sql = "";
			sql = "select " +
			"	vd.id AS DetailID, " +
			"	vd.VentaDatosID AS SellID, " +
			"	ad.id AS ArticleID, " +
			"	ad.Code AS Code, " +
			"	ad.BarCode AS Barcode, " +
			"	ud.Name AS UnitName, " +
			"	vd.Cantidad AS Amount, " +
			"	vd.PrecioBase AS DefaultPrice, " +
			"	vd.PrecioFinal AS FinalPrice, " +
			"	vd.Total AS Total, " +
			"	vd.Rentabilidad AS Profitability " +
			"from (select i.rowid as id, i.* from ventaDetalle as i) AS vd " +
			"inner join articulos AS ad on ad.id = vd.articuloid " +
			"inner join unidades AS ud on ud.id = ad.sellunitid " +
			"where vd.ventaDatosId = ? " +
			"and vd.activo = 1 " +
			"and vd.borrado = 0 " +
			"and ad.active = 1 " +
			"and ud.active = 1 ";
			tx.executeSql(sql, [id], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					result.push({
						ID: row.DetailID,
						SellID: row.SellID,
						ArticleID: row.ArticleID,
						Code: row.Code,
						Barcode: row.Barcode,
						UnitName: row.UnitName,
						Amount: row.Amount,
						DefaultPrice: row.DefaultPrice,
						FinalPrice: row.FinalPrice,
						Total: row.Total,
						Profitability: row.Profitability
					});
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al obtener el detalle de una venta." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};

	this.listByDate = function (fechai, fechae) {
		var q = $q.defer();
		var result = [];

		dbService.openDatabase().transaction(function (tx) {
			var sql="select * " +
					"from vwVentas as vd " +
					"where strftime('%Y%m%d', vd.CreatedDate) between strftime('%Y%m%d', date(?)) and strftime('%Y%m%d', date(?))";
			tx.executeSql(sql, [fechai.toISOString(), fechae.toISOString()], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					result.push(parseOrders(rs.rows.item(i)));
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al obtener listar ventas por periodo." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};

	this.listByCustomer = function (customerId) {
		var q = $q.defer();
		var result = [];

		dbService.openDatabase().transaction(function (tx) {
			var sql = "select * " +
					"from vwVentas as vd " +
					"where vd.CustomerId = ? ";
			tx.executeSql(sql, [customerId], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					result.push(parseOrders(rs.rows.item(i)));
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al obtener listar ventas por cliente." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};

	this.listByStore = function (storeId) {
		var q = $q.defer();
		var result = [];

		dbService.openDatabase().transaction(function (tx) {
			var sql = "select * " +
					"from vwVentas as vd " +
					"where vd.StoreID = ? ";
			tx.executeSql(sql, [storeId], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					result.push(parseOrders(rs.rows.item(i)));
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al obtener listar ventas por tienda." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};

	var parseOrders = function (row) {
		return {
			ID: row.ID,
			Key: row.Key,
			CustomerID: row.CustomerID,
			CustomerName: row.CustomerName,
			CustomerAlias: row.CustomerAlias,
			CustomerTaxID: row.CustomerTaxID,
			CustomerActivity: row.CustomerActivity,
			StoreID: row.StoreID,
			StoreName: row.StoreName,
			StoreManager: row.StoreManager,
			CreatedDate: new Date(row.CreatedDate),
			SendedDate: row.SendedDate,
			Total: row.Total,
			Profitability: row.Profitability,
			IsSended: row.IsSended,
			IsActive: row.IsActive
		};
	};
}]);


appServices.service("routesService", function ($log, $q, dbService, cryptService) {
	this.list = function () {
		var q = $q.defer();
		var result = [];

		dbService.openDatabase().transaction(function (tx) {
			tx.executeSql("select * from vwRutas AS rd", [], function (tx, rs) {
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					result.push({
						ID: row.ID, 
						Name: row.Name, 
						Address: row.Address, Neighborhood: row.Neighborhood, CityName: row.CityName, StateName: row.StateName, ZipCode: row.ZipCode, References: row.References,
						ManagerName: row.ManagerName, eMail: row.eMail, WorkPhone: row.PhoneWork, MobilePhone: row.PhoneMobile,
						CustomerID: row.CustomerID, CustomerAlias: row.CustomerAlias, CustomerName: row.CustomerName, ActivityName: row.ActivityName
					});
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error obtener los datos del cliente." });
		}, function () {
			//onsuccess
			q.resolve(result);
		});

		return q.promise;
	};
});

appServices.service("monitorService",function($log, $q, dbService, cryptService, daoService, $http){

	self = this;

	var createSignature = function (token) {
		return daoService.settingsList.apiKey + "|" + daoService.settingsList.apiUserName + "|" + daoService.settingsList.apiPassword + "|" + token;
	};

	this.uploadResource = function (url,data) {
		var q = $q.defer();

		var time = new Date().valueOf();
		var iUrl = daoService.settingsList.apiUrl + url;
		var sign = createSignature(time);

		/*encriptamos la firma y procedemos con la solicitud $http*/
		var md5 = cryptService.md5(sign).then(function (hash) {
			var api = $http.post(iUrl, {
				UserName: daoService.settingsList.apiUserName,
				Password: daoService.settingsList.apiPassword,
				Token: time,
				Signature: hash,
				Content: data
			});

			api.success(function (r) {
				if (r.CodeNumber == 302  || r.CodeNumber == 200) {
					q.resolve(r);
				} else {
					q.reject(r);
				}
			});

			api.error(function (e) {
				$log.error(e);
				q.reject({ CodeNumber: 504, Message: e });
			});

			api.finally(function () {

			});
		});

		return q.promise;
	};	
	

	this.uploadCustomers = function () {		
		dbService.getCustomersNotSync().then(function (customers){
			angular.forEach(customers, function(v, i){
				self.uploadResource("/Customers/UploadCustomers", v).then(function (r) {
					$log.info("Cliente Enviado: " + v.ID);
				}, function (e) {
					$log.error(e);
				});
			});
		});
	};


	this.uploadStores = function () {
		dbService.getStoresNotSync().then(function (stores){
			angular.forEach(stores, function(v, i){
				self.uploadResource("/Stores/UploadStores", v).then(function (r) {
					$log.info("Envio exitoso de tiendas");
				}, function (e) {
					$log.error(e);
				});
			});
		});
	};

	this.uploadSells = function () {
		dbService.getSellsNotSync().then(function (sells){
			angular.forEach(sells, function(v, i){
					self.uploadResource("/Sells/uploadSells",v).then(function (r) {
						$log.info("Envio exitoso de ventas");
						q.resolve();
					}, function (e) {
						$log.error(e);
					});
			});
		});
	};

	/*this.uploadPayments = function () {
		dbService.getPaymentsNotSync().then(function (sells){
			angular.forEach(payments, function(v, i){
					self.uploadResource("/Payments/uploadPayments",v).then(function (r) {
						$log.info("Envio exitoso de pagos");
						q.resolve();
					}, function (e) {
						$log.error(e);
					});
			});
		});
	};*/

});

appServices.service("invoicesService", function($log, $q, dbService, cryptService){
	this.list = function(){
		var q = $q.defer();
		var invoicesList = [];

		dbService.openDatabase().transaction(function (tx) {

			var sql = "SELECT f.*, c.name AS storeName,(SELECT name FROM clientes cli WHERE cli.id = c.parentid) AS customerName "+
					  "FROM facturas f INNER JOIN clientes c ON c.id = f.storeid " +
					  "WHERE f.active=1 AND f.deleted=0 AND f.sended=0";
			tx.executeSql(sql, [], function (tx, rs) {
				
				for (var i = 0; i < rs.rows.length; i++) {
					var row = rs.rows.item(i);
					invoicesList.push({
						ID: row.id,
						Number: row.number, 
						StoreID: row.storeid, 
						OrderNumber: row.orderNumber, 
						OrderDate: row.orderDate, 
						DeliveryDate: row.deliveryDate, 
						SubTotal: row.subTotal, 
						IVA: row.iva, 
						Total: row.total, 
						PendindPayment: row.pendindPayment,
						StoreName: row.storeName,
						CustomerName: row.customerName
					});
				}
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al obtener listar facturas." });
		}, function () {
			//onsuccess
			q.resolve(invoicesList);
		});

		return q.promise;

	};

	this.find = function(id){
		var q = $q.defer();
		var invoice = null;

		dbService.openDatabase().transaction(function (tx) {
			var sql = "SELECT * FROM facturas f WHERE id = ?";
			tx.executeSql(sql, [id], function (tx, rs) {
				if(rs.rows.length == 1){
					var row = rs.rows.item(0);
					invoice = {
						ID: row.id,
						Number: row.number, 
						StoreID: row.storeid, 
						OrderNumber: row.orderNumber, 
						OrderDate: row.orderDate, 
						DeliveryDate: row.deliveryDate, 
						SubTotal: row.subTotal, 
						IVA: row.iva, 
						Total: row.total, 
						PendindPayment: row.pendindPayment
					};
				};
			});
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al obtener listar facturas." });
		}, function () {
			//onsuccess
			q.resolve(invoice);
		});

		return q.promise;
	};

	this.update = function(invoice){
		var q = $q.defer();

		dbService.openDatabase().transaction(function (tx) {
				var sql = "UPDATE facturas SET number=?, storeid=?, orderNumber=?, orderDate=?, deliveryDate=?, subTotal=?, iva=?, total=?, pendindPayment=? WHERE id=?) ";
				tx.executeSql(sql, [invoice.Number,invoice.StoreID,invoice.OrderNumber,invoice.OrderDate, invoice.DeliveryDate, invoice.SubTotal, invoice.IVA, invoice.Total, invoice.PendindPayment,invoice.id]);
		}, function (e) {
			//onerror
			$log.error(e);
			q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al editar la factura." });
		}, function () {
			//onsuccess
			q.resolve();
		});

		return q.promise;
	};

});

appServices.service("paymentsService", function($log, $q, dbService, cryptService){

	this.save = function(payment, invoice){
		var q = $q.defer();

		cryptService.uuid().then(function (uuid) {
			dbService.openDatabase().transaction(function (tx) {
				var sql = "INSERT INTO pagos (id, paymentMethodID, amount, reference, comments, active, deleted) " +
							  "VALUES (?,?,?,?,?,1,0)";
				tx.executeSql(sql, [uuid,payment.PaymentMethodID,payment.Amount,payment.Reference,payment.Comments]);

				sql = "UPDATE facturas SET pendindPayment=? WHERE id=? ";
				tx.executeSql(sql, [invoice.PendindPayment,invoice.ID]);

			}, function (e) {
				//onerror
				$log.error(e);
				q.reject({ CodeNumber: 500, Message: "Ha ocurrido un error al guardar los datos del pago." });
			}, function () {
				//onsuccess
				q.resolve();
			});
		});


		return q.promise;
	};

});

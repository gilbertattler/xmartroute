var appControllers = angular.module("appControllers", []);
appControllers.controller("defaultController", ["$scope", function ($scope) {

}]);

appControllers.controller("splashController", ["$scope", "splashService", "$state", "notificationService", "$ionicHistory", function ($scope, splashService, $state, notificationService, $ionicHistory) {
	splashService.load().then(function () {
		$state.go("app.customers");
	}, function (e) {
		notificationService.showAlert("Ha ocurrido un error al cargar los datos locales.").then(function () {
			$state.go("app.customers").then(function () {
				//$ionicHistory.clearHistory();
			});
		});
	}).then(function () {
		notificationService.hideLoading();
	});
}]);

appControllers.controller("settingsController", ["$scope", "settingsService", "notificationService", "cryptService", "settingsService", function ($scope, settingsService, notificationService, cryptService, settingsService) {
	//models
	$scope.data = settingsService.getSettings();
	$scope.data.apiPwd = "";

	$scope.clearSettings = function (e) {
		e.preventDefault();
		notificationService.showConfirm("Realmente desea reiniciar todos los valores").then(function (r) {
			if (r) {
				settingsService.clearSettings();
				notificationService.showAlert("Los valores han sido reiniciados correctamente.");
			}
		});
	};

	$scope.saveSettings = function (e) {
		e.preventDefault();
		if ($scope.data.apiKey.trim() === "" || $scope.data.apiKey.trim().length != 32) {
			notificationService.showAlert("No se ha especificado el <strong>ApiKey</strong> o es invalido.");
		} else if ($scope.data.apiUserName.trim() === "") {
			notificationService.showAlert("No se ha especificado el <strong>Usuario</strong> o es invalido.");
		} else if ($scope.data.apiPwd === "") {
			notificationService.showAlert("No se ha especificado la <strong>Contrase&ntilde;a</strong> o es invalida.");
		} else if ($scope.data.apiUrl === "") {
			notificationService.showAlert("No se ha especificado la <strong>Url</strong> o es invalida.");
		} else {
			notificationService.showConfirm("La informaci&oacute;n es correcta").then(function (r) {
				if (r) {
					cryptService.md5($scope.data.apiPwd).then(function (hash) {
						$scope.data.apiPassword = hash;
						$scope.data.apiPwd = "";

						settingsService.saveSettings($scope.data);
						notificationService.showAlert("Los valores han sido almacenados correctamente.");
					});
				}
			});
		}
	};

	$scope.dbSynchronize = function (e) {
		e.preventDefault();
		settingsService.dbSynchronize().then(function () {
			notificationService.showAlert("El dispisitivo ha sido sincronizado correctamente.");
		});
	};
}]);

appControllers.controller("customerSearchController", ["$scope", "$state", "customerSearchService", "notificationService", function ($scope, $state, customerSearchService, notificationService) {
	$scope.data = {
		searchText: ""
	};

	$scope.customersList = [];

	$scope.performSearch = function () {
		if ($scope.data.searchText.trim().length >= 3) {
			customerSearchService.search($scope.data.searchText).then(function (r) {
				$scope.customersList = r;
			}, function (e) {
				notificationService.showAlert("Ha ocurrido un error al realizar la b&uacute;squeda de clientes.");
			}).then(function () {
				$scope.$broadcast('scroll.refreshComplete');
			});
		} else {
			customerSearchService.list().then(function (r) {
				$scope.customersList = r;
			}, function (e) {
				notificationService.showAlert("Ha ocurrido un error al listar los clientes.");
			}).then(function () {
				$scope.$broadcast('scroll.refreshComplete');
			});
		}
	};

	$scope.clearSearch = function (e) {
		e.preventDefault();
		$scope.data.searchText = "";
		$scope.customersList = [];
	};

	$scope.showNewClientForm = function (e) {
		e.preventDefault();
		$state.go("app.customers-edit");
	};

	$scope.editCustomer = function (s, e) {
		$state.go("app.customers-edit", { ID: s.ID });
	};

	$scope.showStores = function (s, e) {
		e.preventDefault();
		$state.go("app.stores", { CustomerID: s.ID });
	};

	$scope.showCustomerOrders = function (s, e) {
		$state.go("app.customerOrders", { CustomerID: s.ID });
	};

	$scope.performSearch();
}]);

appControllers.controller("customersController", ["$scope", "$state", "dataService", "customersService", "notificationService", "$stateParams", function ($scope, $state, dataService, customersService, notificationService, $stateParams) {
	$scope.data = {
		ID: "",
		Name: "", Alias: "", TaxID: "",
		Address: "", Neighborhood: "", CityName: "", StateID: "", ZipCode: "",
		ManagerName: "", eMail: "", WorkPhone: "", MobilePhone: "",
		PriceListID: "", BusinessLineID: ""
	};

	$scope.getPriceList = function () {
		return dataService.priceList;
	};

	$scope.getBusinessLine = function () {
		return dataService.businessLineList;
	};

	$scope.getStates = function () {
		return dataService.statesList;
	};

	$scope.saveCustomer = function () {
		if ($scope.data.Name.trim() == "") {
			notificationService.showAlert("No se ha especificado la <b>Raz&oacute;n Social</b> o es invalida.");
		} else if ($scope.data.Alias.trim() == "") {
			notificationService.showAlert("No se ha especificado el <b>Alias</b> o es invalido.");
		} else if ($scope.data.Address.trim() == "") {
			notificationService.showAlert("No se ha especificado el <b>Domicilio</b> o es invalido.");
		} else if ($scope.data.Neighborhood.trim() == "") {
			notificationService.showAlert("No se ha especificado la <b>Colonia</b> o es invalida.");
		} else if ($scope.data.CityName.trim() == "") {
			notificationService.showAlert("No se ha especificado la <b>Ciudad</b> o es invalida.");
		} else if ($scope.data.StateID == "") {
			notificationService.showAlert("No se ha especificado la <b>Entidad Federativa</b> o es invalido.");
		} else if ($scope.data.ZipCode.trim() == "") {
			notificationService.showAlert("No se ha especificado el <b>C&oacute;digo Postal</b> o es invalido.");
		} else if ($scope.data.ManagerName.trim() == "") {
			notificationService.showAlert("No se ha especificado la <b>Persona de Contacto</b> o es invalida.");
		} else if ($scope.data.WorkPhone.trim() == "") {
			notificationService.showAlert("No se ha especificado el <b>Tel&eacute;fono del Negocio</b> o es invalido.");
		} else if ($scope.data.PriceListID == "") {
			notificationService.showAlert("No se ha especificado la <b>Lista de Precios</b> o es invalida.");
		} else if ($scope.data.BusinessLineID == "") {
			notificationService.showAlert("No se ha especificado el <b>Giro del Negocio</b> o es invalido.");
		} else {
			notificationService.showConfirm("La informaci&oacute;n es correcta").then(function (r) {
				if (r) {
					notificationService.showLoading("Guardando Datos");
					customersService.save($scope.data).then(function () {
						notificationService.showAlert("Los datos del cliente han sido actualizados correctamente.");
					}, function () {
						notificationService.showAlert("Ha ocurrido un error al modificar los datos de un cliente.");
					}).then(function () {
						notificationService.hideLoading();
					});
				}
			});
		}
	};

	var loadCustomerData = function () {
		if (angular.isDefined($stateParams.ID) && $stateParams.ID !== "") {
			notificationService.showLoading("Cargando Datos del Cliente");
			customersService.find($stateParams.ID).then(function (r) {
				if (r === null) {
					notificationService.showAlert("El cliente especificado no existe.", "Error").then(function () {
						$ionicHistory.goBack();
					});
				} else {
					$scope.data = r;
				}
			}, function (e) {
				notificationService.showAlert(e.Message, "Error").then(function () {
					$ionicHistory.goBack();
				});
			}).then(function () {
				notificationService.hideLoading();
			});
		}
	};
	loadCustomerData();
}]);

appControllers.controller("storesController", ["$scope", "$state", "dataService", "$stateParams", "customersService", "notificationService", "storesService", "$ionicHistory", function ($scope, $state, dataService, $stateParams, customersService, notificationService, storesService, $ionicHistory) {
	$scope.storesList = [];
	$scope.customerData = {};

	$scope.showNewStoreForm = function (e) {
		e.preventDefault();
		$state.go("app.stores-new", { CustomerID: $scope.customerData.ID });
	};

	$scope.editStore = function (s, e) {
		e.preventDefault();
		$state.go("app.stores-edit", { ID: s.ID });
	};

	$scope.editCustomer = function (e) {
		e.preventDefault();
		$state.go("app.customers-edit", { ID: $scope.customerData.ID });
	};

	$scope.performSell = function (s, e) {
		e.preventDefault();
		$state.go("app.stores-sell", { StoreID: s.ID });
	};

	$scope.showCustomerOrders = function (s, e) {
		$state.go("app.customerOrders", { CustomerID: $scope.customerData.ID });
	};

	$scope.showStoreOrders = function (s, e) {
		$state.go("app.storeOrders", { StoreID: s.ID });
	};

	var loadCustomerData = function () {
		if (angular.isDefined($stateParams.CustomerID) && $stateParams.CustomerID !== "") {
			notificationService.showLoading("Cargando Datos del Cliente");
			customersService.find($stateParams.CustomerID).then(function (r) {
				if (r === null) {
					notificationService.showAlert("El cliente especificado no existe.", "Error").then(function () {
						$ionicHistory.goBack();
					});
				} else {
					$scope.customerData = r;
					$scope.loadStores();
				}
			}, function (e) {
				notificationService.showAlert(e.Message, "Error").then(function () {
					$ionicHistory.goBack();
				});
			}).then(function () {
				notificationService.hideLoading();
			});
		}
	};
	loadCustomerData();

	$scope.loadStores = function () {
		notificationService.showLoading("Cargando Tiendas.");
		storesService.list($scope.customerData.ID).then(function (r) {
			$scope.storesList = r;
		}, function (e) {
			notificationService.showAlert(e.Message, "Error");
		}).then(function () {
			$scope.$broadcast('scroll.refreshComplete');
			notificationService.hideLoading();
		});
	};
}]);

appControllers.controller("storesEditController", ["$scope", "$state", "dataService", "$stateParams", "customersService", "notificationService", "storesService", "$ionicHistory", function ($scope, $state, dataService, $stateParams, customersService, notificationService, storesService, $ionicHistory) {
	$scope.data = {
		ID: "",
		Name: "", ParentID: "",
		Address: "", Neighborhood: "", CityName: "", StateID: "", ZipCode: "", References: "",
		ManagerName: "", eMail: "", WorkPhone: "", MobilePhone: "",
		WeekDay: null, FrequencyID: null
	};

	$scope.importButtonVisible = false;

	$scope.importData = function (e) {
		notificationService.showLoading("Cargando Datos del Cliente");
		customersService.find($scope.data.ParentID).then(function (r) {
			if (r === null) {
				notificationService.showAlert("El cliente especificado no existe.", "Error").then(function () {
					$ionicHistory.goBack();
				});
			} else {
				$scope.data.Name = r.Alias;
				$scope.data.Address = r.Address; $scope.data.Neighborhood = r.Neighborhood;
				$scope.data.CityName = r.CityName; $scope.data.StateID = r.StateID; $scope.data.ZipCode = r.ZipCode;
				$scope.data.ManagerName = r.ManagerName; $scope.data.eMail = r.eMail; $scope.data.WorkPhone = r.WorkPhone; $scope.data.MobilePhone = r.MobilePhone;
			}
		}, function (e) {
			notificationService.showAlert(e.Message, "Error").then(function () {
				$ionicHistory.goBack();
			});
		}).then(function () {
			notificationService.hideLoading();
		});
	};

	$scope.getWeekDays = function () {
		return dataService.dayList;
	};

	$scope.getFrequenciesList = function () {
		return dataService.frequenciesList;
	};

	$scope.getStates = function () {
		return dataService.statesList;
	};

	$scope.saveStore = function () {
		if ($scope.data.Name.trim() == "") {
			notificationService.showAlert("No se ha especificado el <b>Nombre</b> o es invalida.");
		} else if ($scope.data.Address.trim() == "") {
			notificationService.showAlert("No se ha especificado el <b>Domicilio</b> o es invalido.");
		} else if ($scope.data.Neighborhood.trim() == "") {
			notificationService.showAlert("No se ha especificado la <b>Colonia</b> o es invalida.");
		} else if ($scope.data.CityName.trim() == "") {
			notificationService.showAlert("No se ha especificado la <b>Ciudad</b> o es invalida.");
		} else if ($scope.data.StateID == "") {
			notificationService.showAlert("No se ha especificado la <b>Entidad Federativa</b> o es invalido.");
		} else if ($scope.data.ZipCode.trim() == "") {
			notificationService.showAlert("No se ha especificado el <b>C&oacute;digo Postal</b> o es invalido.");
		} else if ($scope.data.References.trim() == "") {
			notificationService.showAlert("No se han especificado las <b>Entre Calles</b> o son invalidas.");
		} else if ($scope.data.ManagerName.trim() == "") {
			notificationService.showAlert("No se ha especificado la <b>Persona de Contacto</b> o es invalida.");
		} else if ($scope.data.WorkPhone.trim() == "") {
			notificationService.showAlert("No se ha especificado el <b>Tel&eacute;fono del Negocio</b> o es invalido.");
		} else if ($scope.data.WeekDay == null) {
			notificationService.showAlert("No se ha especificado el <b>D&iacute;a de Visita</b> o es invalido.");
		} else if ($scope.data.FrequencyID == null) {
			notificationService.showAlert("No se ha especificado la <b>Frecuenciaa de Visita</b> o es invalida.");
		} else {
			notificationService.showConfirm("La informaci&oacute;n es correcta").then(function (r) {
				if (r) {
					notificationService.showLoading("Guardando Informaci&oacute;n");
					storesService.save($scope.data).then(function () {
						notificationService.showAlert("La tienda ha sido actualizada correctamente.");
					}, function () {
						notificationService.showAlert("Ha ocurrido un error al guardar los datos de la tienda.");
					}).then(function () {
						notificationService.hideLoading();
					});
				}
			});
		}
	};

	if (angular.isDefined($stateParams.CustomerID) && $stateParams.CustomerID !== "") {
		$scope.data.ParentID = $stateParams.CustomerID;
		$scope.importButtonVisible = true;
	} else if (angular.isDefined($stateParams.ID) && $stateParams.ID !== "") {
		notificationService.showLoading("Cargando informaci&oacute;n de la Tienda.");
		storesService.find($stateParams.ID).then(function (r) {
			$scope.data = r;
		}, function (e) {
			notificationService.showAlert("Ha ocurrido un error y no ha sido posible localizar la tienda especificada o no existe.").then(function () {
				$ionicHistory.goBack();
			});
		}).then(function () {
			notificationService.hideLoading();
		});
	} else {
		notificationService.showAlert("Ha ocurrido un error y no ha sido posible identificar el cliente.", "Error").then(function () {
			$ionicHistory.goBack();
		});
	}
}]);

app.controller("orderEditController", ["$scope", "$state", "$stateParams", "customersService", "storesService", "productsService", "ordersService", "notificationService", "$ionicHistory", "$ionicModal", "$ionicPopup", "$filter", function ($scope, $state, $stateParams, customersService, storesService, productsService, ordersService, notificationService, $ionicHistory, $ionicModal, $ionicPopup, $filter) {
	$scope.storeData = {};
	$scope.customerData = {};

	$scope.searchOptions = { text: "" };
	$scope.searchResults = [];

	$scope.selectedProducts = [];
	$scope.orderOptions = { adminComments: "", financialComments: "", deliveryComments: "" };

	$scope.searchProduct = function (e) {
		e.preventDefault();
		$scope.searchOptions.formatedText = $scope.searchOptions.text.replace(/[^\w\s]+/gi, "").trim();
		$scope.searchOptions.text = "";

		if ($scope.searchOptions.formatedText == "") {
			notificationService.showAlert("Debe especificar la descripci&oacute;n del producto.");
		} else {
			notificationService.showLoading("Buscando productos");
			productsService.search($scope.customerData.ID, $scope.searchOptions.formatedText).then(function (r) {
				if (r.length > 0) {
					$scope.searchResults = r;
					$scope.mwProductSearch.show();
				} else {
					notificationService.showAlert("No existen productos con los datos especificados.");
				}
			}, function (e) {
				notificationService.showAlert("Ha ocurrido un error al buscar el producto.");
			}).then(function () {
				notificationService.hideLoading();
			});
		}
	};

	$scope.closeSearch = function () {
		$scope.mwProductSearch.hide();
	};

	$scope.closeConfirmationWindow = function () {
		$scope.mwConfirmationWindow.hide();
	};

	$scope.confirmOrder = function (e) {
		e.preventDefault();

		var itemEmpty = false;
		angular.forEach($scope.selectedProducts, function (v, i) {
			if (v.Amount <= 0) {
				itemEmpty = true;
			}
		});

		if (itemEmpty) {
			notificationService.showAlert("Existe uno o m&aacute;s producto con cantidad cero.");
		} else if ($scope.selectedProducts.length == 0) {
			notificationService.showAlert("Debe especificar al menos un producto");
		} else if ($scope.getTotal().Total == 0) {
			notificationService.showAlert("La venta no puede ser igual a cero");
		} else {
			$scope.mwConfirmationWindow.show();
		}
	};

	$scope.saveOrder = function (e) {
		e.preventDefault();
		$scope.mwConfirmationWindow.hide().then(function () {
			notificationService.showConfirm("La informaci&oacute;n es correcta").then(function (r) {
				if (r) {
					notificationService.showLoading("Guardando Datos");
					var totales = $scope.getTotal();
					ordersService.saveOrder({
						CustomerID: $scope.storeData.ParentID,
						StoreID: $scope.storeData.ID,
						Total: totales.Total,
						Profitability: totales.Profitability,
						SalesComments: $scope.orderOptions.adminComments,
						FinancialComments: $scope.orderOptions.financialComments,
						DeliveryComments: $scope.orderOptions.deliveryComments,
						Items: $scope.selectedProducts
					}).then(function (id) {
						notificationService.showAlert("La venta ha sido registrada exitosamente.").then(function () {
							$ionicHistory.nextViewOptions({
								disableBack: true
							});
							$state.go("app.orders-show", { OrderID: id });
						});
					}, function (e) {
						notificationService.showAlert("Ha ocurrido un error al almacenar la venta.");
					}).then(function () {
						notificationService.hideLoading();
					});
				}
			});
		});
	};

	$ionicModal.fromTemplateUrl("templates/order-product.html", {
		scope: $scope
	}).then(function (modal) {
		$scope.mwProductSearch = modal;
	});

	$ionicModal.fromTemplateUrl("templates/order-confirmation.html", {
		scope: $scope
	}).then(function (modal) {
		$scope.mwConfirmationWindow = modal;
	});

	$scope.$on('$destroy', function () {
		$scope.mwProductSearch.remove();
		$scope.mwConfirmationWindow.remove();
	});


	$scope.addProduct = function (s, e) {
		e.preventDefault();
		if ($filter("filter")($scope.selectedProducts, { ID: s.ID }).length == 0) {
			s.CurrentPrice = s.DefaultPrice;
			$scope.selectedProducts.push(s);
			$scope.closeSearch();
		} else {
			notificationService.showAlert("El producto seleccionado ya se encuentra en el carrito de compras.");
		}
	};

	$scope.changePrice = function (s, e) {
		e.preventDefault();
		$scope.tmp = { amount: s.Amount, price: s.CurrentPrice };
		$ionicPopup.show({
			template: '<input type="number" ng-model="tmp.price" style="text-align:center">',
			title: 'Ingrese el Precio Final',
			subTitle: 'Solo se permiten <b>n&uacute;meros</b>',
			scope: $scope,
			buttons: [
			  { text: 'Cancelar' },
			  {
			  	text: 'Guardar',
			  	type: 'button-positive',
			  	onTap: function (n, i) {
			  		if (isNaN($scope.tmp.price) || $scope.tmp.price <= 0) {
			  			n.preventDefault();
			  		} else {
			  			s.CurrentPrice = $scope.tmp.price;
			  			s.Profitability = parseFloat((((s.CurrentPrice / s.Cost) * 100) - 100).toFixed(2));
			  		}
			  	}
			  }
			]
		});
	};

	$scope.changeAmount = function (s, e) {
		e.preventDefault();
		$scope.tmp = { amount: s.Amount };
		$ionicPopup.show({
			template: '<input type="number" ng-model="tmp.amount" style="text-align:center">',
			title: 'Ingrese Cantidad de Art&iacute;culos',
			subTitle: 'Solo se permiten <b>n&uacute;meros</b>',
			scope: $scope,
			buttons: [
			  { text: 'Cancelar' },
			  {
			  	text: 'Guardar',
			  	type: 'button-positive',
			  	onTap: function (n) {
			  		if (isNaN($scope.tmp.amount) || $scope.tmp.amount <= 0) {
			  			n.preventDefault();
			  		} else {
			  			s.Amount = $scope.tmp.amount;
			  		}
			  	}
			  }
			]
		});
	};

	$scope.removeProduct = function (s, e) {
		e.preventDefault();
		notificationService.showConfirm("Desea eliminar el producto").then(function (a) {
			if (a) {
				$scope.selectedProducts.splice($scope.selectedProducts.indexOf(s));
			}
		});
	};

	$scope.getTotal = function () {
		var result = { Total: 0, TotalBase: 0, Profitability: 0 };
		angular.forEach($scope.selectedProducts, function (v, i) {
			result.Total += parseFloat((v.CurrentPrice * v.Amount).toFixed(4));
			result.TotalBase += parseFloat((v.Cost * v.Amount).toFixed(4));
		});

		if (result.TotalBase == 0) {
			result.Profitability = 0;
		} else {
			result.Profitability = parseFloat((((result.Total / result.TotalBase) * 100) - 100).toFixed(2));
		}

		return result;
	};



	if (angular.isDefined($stateParams.StoreID)) {
		notificationService.showLoading("Cargando informaci&oacute;n de la Tienda.");
		storesService.find($stateParams.StoreID).then(function (r) {
			$scope.storeData = r;

			notificationService.showLoading("Cargando informaci&oacute;n del Cliente.");
			customersService.find(r.ParentID).then(function (c) {
				$scope.customerData = c;
			}, function (e) {
				notificationService.showAlert("Ha ocurrido un error y no ha sido posible localizar al cliente especificado o no existe.").then(function () {
					$ionicHistory.goBack();
				});
			}).then(function () {
				notificationService.hideLoading();
			});
		}, function (e) {
			notificationService.showAlert("Ha ocurrido un error y no ha sido posible localizar la tienda especificada o no existe.").then(function () {
				notificationService.hideLoading();
				$ionicHistory.goBack();
			});
		});
	} else {
		$ionicHistory.goBack();
	}
}]);

appControllers.controller("orderShowController", function ($scope, $state, $stateParams, customersService, storesService, ordersService, notificationService) {
	$scope.storeData = {};
	$scope.customerData = {};
	$scope.orderData = {};
	$scope.orderItems = [];

	var controller_load = function () {
		if (angular.isDefined($stateParams.OrderID)) {
			var orderId = $stateParams.OrderID;
			notificationService.showLoading("Cargando Informaci&oacute;n de la Venta");
			ordersService.find(orderId).then(function (r) {
				if (r.length == 0) {
					notificationService.hideLoading();
					notificationService.showAlert("La venta especificada no existe").then(function () {
						$state.go("app.orders-list");
					});
				} else {
					$scope.orderData = r;
					notificationService.showLoading("Cargando Detalle de la Venta");
					ordersService.getItems(orderId).then(function (items) {
						$scope.orderItems = items;

						notificationService.showLoading("Cargando Informaci&oacute;n de la Tienda");
						storesService.find($scope.orderData.StoreID).then(function (r) {
							$scope.storeData = r;

							notificationService.showLoading("Cargando informaci&oacute;n del Cliente.");
							customersService.find(r.ParentID).then(function (c) {
								$scope.customerData = c;
							}, function (e) {
								notificationService.showAlert("Ha ocurrido un error y no ha sido posible localizar al cliente especificado o no existe.").then(function () {
									$state.go("app.orders-list");
								});
							}).then(function () {
								notificationService.hideLoading();
							});
						}, function (e) {
							notificationService.hideLoading();
							notificationService.showAlert("Ha ocurrido un error y no ha sido posible localizar la tienda especificada o no existe.").then(function () {
								$state.go("app.orders-list");
							});
						});
					}, function (e) {
						notificationService.hideLoading();
						notificationService.showAlert("Ha ocurrido un error al cargar los articulos de la venta").then(function () {
							$state.go("app.orders-list");
						});
					});
				}
			}, function (e) {
				notificationService.hideLoading();
				notificationService.showAlert("Ha ocurrido un error al cargar la venta").then(function () {
					$state.go("app.orders-list");
				});
			});
		}
	};

	controller_load();
});

appControllers.controller("ordersController", function ($scope, $state, $stateParams, notificationService, ordersService) {
	$scope.sellsList = [];

	$scope.showOrder = function (s, e) {
		$state.go("app.orders-show", { OrderID: s.ID });
	};

	$scope.showCustomer = function (s, e) {
		$state.go("app.customerOrders", { CustomerID: s.CustomerID });
	};

	$scope.showStoreSells = function (s, e) {
		$state.go("app.storeOrders", { StoreID: s.StoreID });
	};

	$scope.loadData = function () {
		notificationService.showLoading("Cargando Listado de Ventas del Periodo");
		ordersService.listByDate(new Date(), new Date()).then(function (r) {
			$scope.sellsList = r;
		}, function (e) {
			notificationService.showAlert("Ha ocurrido un error al listas las ventas del periodo.");
		}).then(function () {
			$scope.$broadcast('scroll.refreshComplete');
			notificationService.hideLoading();
		});
	};
	$scope.loadData();
});

appControllers.controller("customerOrdersController", function ($scope, $state, $stateParams, notificationService, ordersService, customersService, $ionicHistory) {
	$scope.sellsList = [];
	$scope.customerData = {};

	$scope.showOrder = function (s, e) {
		$state.go("app.orders-show", { OrderID: s.ID });
	};

	$scope.editCustomer = function (e) {
		$state.go("app.customers-edit", { ID: $scope.customerData.ID });
	};

	$scope.showStores = function (e) {
		$state.go("app.stores", { CustomerID: $scope.customerData.ID });
	};

	$scope.showStoreSells = function (s, e) {
		$state.go("app.storeOrders", { StoreID: s.StoreID });
	};

	$scope.loadData = function () {
		notificationService.showLoading("Cargando Listado de Ventas del Cliente");
		ordersService.listByCustomer($stateParams.CustomerID).then(function (r) {
			$scope.sellsList = r;
		}, function (e) {
			notificationService.showAlert("Ha ocurrido un error al listas las ventas del cliente.");
		}).then(function () {
			$scope.$broadcast('scroll.refreshComplete');
			notificationService.hideLoading();
		});
	};
	
	var loadCustomerData = function () {
		if (angular.isDefined($stateParams.CustomerID) && $stateParams.CustomerID !== "") {
			notificationService.showLoading("Cargando Datos del Cliente");
			customersService.find($stateParams.CustomerID).then(function (r) {
				if (r === null) {
					notificationService.showAlert("El cliente especificado no existe.", "Error").then(function () {
						notificationService.hideLoading();
						$ionicHistory.goBack();
					});
				} else {
					$scope.customerData = r;
					$scope.loadData();
				}
			}, function (e) {
				notificationService.showAlert(e.Message, "Error").then(function () {
					notificationService.hideLoading();
					$ionicHistory.goBack();
				});
			});
		}
	};
	loadCustomerData();
});

appControllers.controller("storeOrdersController", function ($scope, $state, $stateParams, notificationService, ordersService, customersService, storesService, $ionicHistory) {
	$scope.sellsList = [];
	$scope.customerData = {};
	$scope.storeData = {};

	$scope.showOrder = function (s, e) {
		$state.go("app.orders-show", { OrderID: s.ID });
	};

	$scope.showCustomer = function (s, e) {
		$state.go("app.customerOrders", { CustomerID: s.CustomerID });
	};

	$scope.editCustomer = function (e) {
		$state.go("app.customers-edit", { ID: $scope.customerData.ID });
	};

	$scope.performSell = function (e) {
		$state.go("app.stores-sell", { StoreID: $scope.storeData.ID });
	};

	$scope.editStore = function (e) {
		$state.go("app.stores-edit", { ID: $scope.storeData.ID });
	};

	$scope.showStores = function (e) {
		$state.go("app.stores", { CustomerID: $scope.customerData.ID });
	};


	$scope.loadData = function () {
		notificationService.showLoading("Cargando Listado de Ventas de la Tienda");
		ordersService.listByStore($stateParams.StoreID).then(function (r) {
			$scope.sellsList = r;
		}, function (e) {
			notificationService.showAlert("Ha ocurrido un error al listas las ventas de la tienda.");
		}).then(function () {
			$scope.$broadcast('scroll.refreshComplete');
			notificationService.hideLoading();
		});
	};

	var loadCustomerData = function (id) {
		notificationService.showLoading("Cargando Datos del Cliente");
		customersService.find(id).then(function (r) {
			if (r === null) {
				notificationService.showAlert("El cliente especificado no existe.", "Error").then(function () {
					notificationService.hideLoading();
					$ionicHistory.goBack();
				});
			} else {
				$scope.customerData = r;
				$scope.loadData();
			}
		}, function (e) {
			notificationService.showAlert(e.Message, "Error").then(function () {
				notificationService.hideLoading();
				$ionicHistory.goBack();
			});
		});
	};
	
	var loadStoreData = function () {
		if (angular.isDefined($stateParams.StoreID) && $stateParams.StoreID !== "") {
			notificationService.showLoading("Cargando Datos de la Tienda");
			storesService.find($stateParams.StoreID).then(function (r) {
				$scope.storeData = r;
				loadCustomerData(r.ParentID);
			}, function (e) {
				notificationService.showAlert("Ha ocurrido un error y no ha sido posible localizar la tienda especificada o no existe.").then(function () {
					notificationService.hideLoading();
					$ionicHistory.goBack();
				});
			});
		} else {
			$ionicHistory.goBack();
		}
	};

	loadStoreData();
});

appControllers.controller("invoicesController", function ($scope, notificationService, invoicesService, $state) {
	$scope.invoicesList = [];

	$scope.loadInvoices = function () {
		invoicesService.list().then(function (r) {
			$scope.invoicesList = r;
		}, function (e) {
			notificationService.showAlert("Ha ocurrido un error al listar las facturas.");
		}).then(function () {
			$scope.$broadcast('scroll.refreshComplete');
		});
	};

	$scope.showNewPaymentForm = function (s, e) {
		if(s.PendindPayment == 0){
			notificationService.showAlert("Ya no se tiene adeudo en la factura seleccionada.").then(function () {
			notificationService.hideLoading();
			});
		} else{
			$state.go("app.payments", { InvoiceID: s.ID });
		}
	};

	$scope.loadInvoices();

});

appControllers.controller("routesController", function ($scope, $state, notificationService, routesService) {
	$scope.routesList = [];

	$scope.editStore = function (s, e) {
		e.preventDefault();
		$state.go("app.stores-edit", { ID: s.ID });
	};

	$scope.performSell = function (s, e) {
		e.preventDefault();
		$state.go("app.stores-sell", { StoreID: s.ID });
	};

	$scope.showStoreOrders = function (s, e) {
		e.preventDefault();
		$state.go("app.storeOrders", { StoreID: s.ID });
	};

	$scope.loadRoutes = function () {
		notificationService.showLoading("Cargando Rutas");
		routesService.list().then(function (r) {
			$scope.routesList = r;
		}, function (e) {
			notificationService.showAlert("Ha ocurrido un error al cargar las rutas");
		}).then(function () {
			notificationService.hideLoading();
			$scope.$broadcast('scroll.refreshComplete');
		});
	};
	$scope.loadRoutes();

});

appControllers.controller("monitorController", 
	function($scope, $timeout, monitorService){

	var sleep = 5000;//5*60*1000;
	var watcher = function(){};

	$scope.start = function(){
		watcher = $timeout(function(){
			monitorService.uploadCustomers();
			monitorService.uploadStores();
			monitorService.uploadSells();
			//monitorService.uploadPayments();
		}, sleep);
	};

	$scope.stop = function(){
		watcher();
	};

	$scope.start();
});

appControllers.controller("paymentsController",
	function($scope, dataService, $state, $stateParams, 
		notificationService, customersService, storesService, 
		$ionicHistory, $filter, invoicesService, paymentsService){

	$scope.payment = 
	{
		Amount: "0.00",
		PaymentMethodID: "00",
		Reference: "",
		InvoiceOrderNumber: "",
		InvoiceNumber: "",
		Comments:""
	};

	$scope.invoice = [];

	$scope.invoice.PendindPayment = "0.00";

	$scope.getPaymentMethodsList = function () {
		return dataService.paymentMethods;
	};

	var loadData = function () {
		notificationService.showLoading("Cargando la información de la factura");
		invoicesService.find($stateParams.InvoiceID).then(function (r) {
			$scope.invoice = r;
			$scope.payment.Amount = r.PendindPayment;
			loadStoreData(r.StoreID)
		}, function (e) {
			notificationService.showAlert("Ha ocurrido un error al consultar la factura.");
		}).then(function () {
		});
	};

	var loadCustomerData = function (id) {
		notificationService.showLoading("Cargando Datos del Cliente");
		customersService.find(id).then(function (r) {
			if (r === null) {
				notificationService.showAlert("El cliente especificado no existe.", "Error").then(function () {
					notificationService.hideLoading();
					$ionicHistory.goBack();
				});
			} else {
				$scope.customerData = r;
				notificationService.hideLoading();
			}
		}, function (e) {
			notificationService.showAlert(e.Message, "Error").then(function () {
				notificationService.hideLoading();
				$ionicHistory.goBack();
			});
		});
	};
	
	var loadStoreData = function () {
		if (angular.isDefined($scope.invoice.StoreID) && $scope.invoice.StoreID !== "") {
			notificationService.showLoading("Cargando Datos de la Tienda");
			storesService.find($scope.invoice.StoreID).then(function (r) {
				$scope.storeData = r;
				loadCustomerData(r.ParentID);
			}, function (e) {
				notificationService.showAlert("Ha ocurrido un error y no ha sido posible localizar la tienda especificada o no existe.").then(function () {
					notificationService.hideLoading();
					$ionicHistory.goBack();
				});
			});
		} else {
			notificationService.showAlert("Ha ocurrido un error y no ha sido posible localizar la tienda especificada o no existe.").then(function () {
				notificationService.hideLoading();
				$ionicHistory.goBack();
			});
		}
	};

	$scope.getPendindPayment = function () {
		var result = 0.00;

		result = parseFloat($scope.invoice.PendindPayment) - (+$scope.payment.Amount);

		return result;
	};

	$scope.savePayment = function () {
		var pendind = $scope.getPendindPayment();

		if ($scope.payment.Reference.trim() == "" && $scope.payment.PaymentMethodID !== "00") {
			notificationService.showAlert("No se ha especificado la <b>Referencia</b> o es invalida.");
		} else if (isNaN($scope.payment.Amount) && !angular.isNumber(+$scope.payment.Amount)){
			notificationService.showAlert("El valor del <b>Monto</b> es invalido.");
		} else if ((+$scope.payment.Amount) <= 0) {
			notificationService.showAlert("No se ha especificado el <b>Monto</b>.");
		} else if (pendind < 0) {
			notificationService.showAlert("El <b>Monto</b> es más que el adeudo.");
		} else if ($scope.invoice.ID.trim() == "") {
			notificationService.showAlert("No existe referencia a la <b>Factura</b>.");
		} else if ((+$scope.invoice.PendindPayment) == 0) {
			notificationService.showAlert("No se tiene ningun <b>adeudo.</b>");
		}
		else {
			notificationService.showConfirm("La informaci&oacute;n es correcta").then(function (r) {
				if (r) {
					$scope.invoice.PendindPayment = pendind;
					$scope.payment.Reference = $scope.payment.PaymentMethodID == "00" ?  "" : $scope.payment.Reference;
					notificationService.showLoading("Guardando Informaci&oacute;n");
					paymentsService.save($scope.payment, $scope.invoice).then(function () {
						notificationService.hideLoading();
						notificationService.showAlert("Se realiz&oacute; la operaci&oacute;n correctamente.").then(function () {
							notificationService.hideLoading();
							$ionicHistory.goBack();
						});
					}, function () {
						notificationService.showAlert("Ha ocurrido un error al guardar los datos.");
					});
				}
			});
		}
	};

	loadData();
});
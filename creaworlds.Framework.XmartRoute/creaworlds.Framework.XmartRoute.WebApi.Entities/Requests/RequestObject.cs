﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Requests
{
	public class RequestObject<T> : RequestBase
	{
		public T Content { get; set; }
	}
}

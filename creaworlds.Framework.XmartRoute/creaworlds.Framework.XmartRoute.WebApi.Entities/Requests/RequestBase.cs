﻿using creaworlds.Framework.XmartRoute.WebApi.Interfaces.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Requests
{
	[Serializable]
	public abstract class RequestBase : IRequest
	{
		public string UserName { get; set; }
		public string Password { get; set; }
		public string Token { get; set; }
		public string Signature { get; set; }
	}
}

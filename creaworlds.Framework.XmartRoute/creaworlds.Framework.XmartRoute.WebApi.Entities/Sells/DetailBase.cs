﻿
namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Sells
{
    public abstract class DetailBase
    {  
        public string ArticleID { get; set; }
        public decimal? Amount { get; set; } 
        public decimal? FinalPrice { get; set; }
        public decimal? Total { get; set; }
        public decimal? Profitability { get; set; }
    }
}


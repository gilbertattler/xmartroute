using System;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Sells
{
	public abstract class SellBase
	{
		public string CustomerID { get; set; }
		public string StoreID { get; set; }
		public DateTime? CreatedDate { get; set; }
		public decimal? Total { get; set; }
		public decimal? Profitability { get; set; }
        public string SellsComments { get; set; }
        public string DeliveryComments { get; set; }
        public string FinancialComments { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Sells
{
    public sealed class DetailUpload : DetailBase
    {
        public string ID { get; set; }
        /// <summary>
        /// Precio Base
        /// </summary>
        public decimal? RegularPrice { get; set; }
    }
}

﻿using System;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Sells
{
    public sealed class SellData:SellBase
    {
        public string ID { get; set; }

        /// <summary>
        /// Fecha en que se recibio la orden desde el dispositivo móvil
        /// </summary>
        public DateTime? UploadedDate { get; set; }

        /// <summary>
        /// Fecha en que se envío a SAP
        /// </summary>
        public DateTime? SynchronizedDate { get; set; }

        /// <summary>
        /// Fecha en que se recibio la confirmación de SAP
        /// </summary>
        public DateTime? ConfirmationDate { get; set; }

        /// <summary>
        /// Número de Factura generado por SAP
        /// </summary>
        public string InvoiceNumber { get; set; }

        public DetailData[] Items { get; set; }

    }
}

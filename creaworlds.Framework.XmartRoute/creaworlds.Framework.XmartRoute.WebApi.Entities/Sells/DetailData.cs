﻿
namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Sells
{
    public sealed class DetailData:DetailBase
    {
        public string ID { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? Taxes { get; set; }
    }
}

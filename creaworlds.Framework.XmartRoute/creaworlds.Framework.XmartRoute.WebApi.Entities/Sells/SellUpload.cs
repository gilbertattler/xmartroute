﻿
namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Sells
{
    public sealed class SellUpload:SellBase
    {
        public string ID { get; set; }
        public DetailUpload[] Items { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Customers
{
	public sealed class CustomerData : CustomerBase
	{
		public string ID { get; set; }

        /// <summary>
		/// Identifies the credit limit for customer
		/// </summary>
		public decimal? CreditLimit { get; set; }

        /// <summary>
        /// Identifies the credit used for customer
        /// </summary>
        public decimal? CreditUsed { get; set; }

        /// <summary>
        /// Identifies credit availible for customer
        /// </summary>
        public decimal? CreditAvailible { get; set; }

        /// <summary>
        /// Fecha en que se recibio la orden desde el dispositivo móvil
        /// </summary>
        public DateTime? UploadedDate { get; set; }

        /// <summary>
        /// Fecha en que se envío a SAP
        /// </summary>
        public DateTime? SynchronizedDate { get; set; }

    }
}

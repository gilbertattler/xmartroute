﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Customers
{
    public abstract class  StoreBase
    {
        /// <summary>
        /// Identifies the Customer Type. E.g. CustomerData, Store, Etc.
        /// </summary>
        public string TypeID { get; set; }

        /// <summary>
        /// Identifies the parent customer id
        /// </summary>
        public string ParentID { get; set; }

        /// <summary>
        /// Business Name of Customer
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Identifies the customer's address for invoice.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Identifies the customer's Neighborhood for invoice
        /// </summary>
        public string Neighborhood { get; set; }

        /// <summary>
        /// Identifies the customer's city name for invoice
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// Identifies the customer's state name for invoice
        /// </summary>
        public string StateName { get; set; }

        /// <summary>
        /// Identifies the customer's country name for invoice
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// Identifies the customer's zipcode for invoice
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Identifies the customer's visiting day. E.g. 00 as sunday, 01 as monday, etc.
        /// Identifies Store only
        /// </summary>
        public string VisitingDay { get; set; }

        /// <summary>
        /// Identifies the customer's work phone for invoice
        /// </summary>
        public string PhoneWork { get; set; }

        /// <summary>
        /// Identifies the customer's mobile phone for invoice
        /// </summary>
        public string PhoneMobile { get; set; }

        /// <summary>
        /// Identifies the customer's email for tracking
        /// </summary>
        public string eMail { get; set; }

        /// <summary>
        /// Identifies if the customer is active or blocked
        /// </summary>
        public bool? IsActive { get; set; }

        ///<summary>
        /// Refers of Store
        /// </summary>
        public string Refers { get; set; }

        ///<summary>
        /// Identifies Frequency of visits
        /// </summary>
        public string FrequencyId { get; set; }

        ///<summary>
        ///Business Manager
        /// </summary>
        public string ManagerName { get; set; }
    }
}

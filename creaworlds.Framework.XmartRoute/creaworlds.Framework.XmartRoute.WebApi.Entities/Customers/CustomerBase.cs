﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Customers
{
	public abstract class CustomerBase
	{
		/// <summary>
		/// Identifies the Customer Type. E.g. CustomerData, Store, Etc.
		/// </summary>
		public string TypeID { get; set; }

		/// <summary>
		/// Business Name of Customer
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Identifies the legal Identification number of this Entity.
		/// </summary>
		public string TaxID { get; set; }

		/// <summary>
		/// Identifies the legal name of the customer.
		/// </summary>
		public string LegalName { get; set; }

		/// <summary>
		/// Identifies the customer's address for invoice.
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// Identifies the customer's Neighborhood for invoice
		/// </summary>
		public string Neighborhood { get; set; }

		/// <summary>
		/// Identifies the customer's city name for invoice
		/// </summary>
		public string CityName { get; set; }

		/// <summary>
		/// Identifies the customer's state name for invoice
		/// </summary>
		public string StateName { get; set; }

		/// <summary>
		/// Identifies the customer's country name for invoice
		/// </summary>
		public string CountryName { get; set; }

		/// <summary>
		/// Identifies the customer's zipcode for invoice
		/// </summary>
		public string ZipCode { get; set; }

		/// <summary>
		/// Identifies the customer's work phone for invoice
		/// </summary>
		public string PhoneWork { get; set; }

		/// <summary>
		/// Identifies the customer's mobile phone for invoice
		/// </summary>
		public string PhoneMobile { get; set; }

		/// <summary>
		/// Identifies the customer's email for tracking
		/// </summary>
		public string eMail { get; set; }

		/// <summary>
		/// Identifies if the customer is a Credit Customer
		/// </summary>
		public bool? IsCreditCustomer { get; set; }

		/// <summary>
		/// Identifies customer's business activity
		/// </summary>
		public string ActivityID { get; set; }

		/// <summary>
		/// Identifies if the customer is active or blocked
		/// </summary>
		public bool? IsActive { get; set; }

        ///<summary>
        ///Store Manager name
        /// </summary>
        public string ManagerName { get; set; }

	}
}

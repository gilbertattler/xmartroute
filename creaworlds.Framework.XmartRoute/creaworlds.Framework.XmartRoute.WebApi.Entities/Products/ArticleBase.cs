﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Products
{
	public abstract class ArticleBase
	{
		public string ProductID { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public string BarCode { get; set; }
		public string SellUnitID { get; set; }
		public string StockUnitID { get; set; }
		public decimal Quantity { get; set; }
		public bool? IsPrimary { get; set; }
		public bool? IsActive { get; set; }
	}
}

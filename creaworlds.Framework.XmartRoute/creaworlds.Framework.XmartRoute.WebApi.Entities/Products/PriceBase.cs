﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Products
{
	public abstract class PriceBase
	{
		public string CompanyID { get; set; }
		public string StoreID { get; set; }
		public string PriceListID { get; set; }
		public string ArticleID { get; set; }
		public decimal? Cost { get; set; }
		public decimal? Base { get; set; }
		public decimal? Taxes { get; set; }
		public decimal? Price { get; set; }
		public decimal? Profitability { get; set; }
		public bool? IsActive { get; set; }
	}
}

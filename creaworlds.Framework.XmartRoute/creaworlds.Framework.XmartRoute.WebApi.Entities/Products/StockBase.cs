﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Products
{
	public abstract class StockBase
	{
		public string CompanyID { get; set; }
		public string StoreID { get; set; }
		public string ProductID { get; set; }
		public string ArticleID { get; set; }
		public DateTime UpdatedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Products
{
	public abstract class ProductTaxBase
	{
		public string ProductID { get; set; }
		public string TaxID { get; set; }
	}
}

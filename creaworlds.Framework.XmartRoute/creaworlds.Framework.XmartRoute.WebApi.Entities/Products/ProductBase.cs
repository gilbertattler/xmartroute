﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Entities.Products
{
	public abstract class ProductBase
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string ConsumptionLevel { get; set; }
		public string UnitID { get; set; }
		public string CategoryID { get; set; }
		public string ParentCategoryID { get; set; }
		public bool? IsActive { get; set; }
	}
}

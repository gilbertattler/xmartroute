﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.IDOC.Tools
{
	public static partial class Parsers
	{
		public static string HeaderBuilder(Dictionary<string, string> data)
		{
			return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15}{16}{17}{18}{19}{20}{21}{22}{23}{24}{25}{26}{27}{28}{29}{30}{31}{32}{33}{34}{35}", new string[] {
				Common.DictionaryValueExtractor("TABNAM", " ", 10, ref data),
				Common.DictionaryValueExtractor("MANDT", " ", 3, ref data),
				Common.DictionaryValueExtractor("DOCNUM", " ", 16, ref data),
				Common.DictionaryValueExtractor("DOCREL", " ", 4, ref data),
				Common.DictionaryValueExtractor("STATUS", " ", 2, ref data),
				Common.DictionaryValueExtractor("DIRECT", " ", 1, ref data),
				Common.DictionaryValueExtractor("OUTMOD", " ", 1, ref data),
				Common.DictionaryValueExtractor("EXPRSS", " ", 1, ref data),
				Common.DictionaryValueExtractor("TEST", " ", 1, ref data),
				Common.DictionaryValueExtractor("IDOCTYP", " ", 30, ref data),
				Common.DictionaryValueExtractor("CIMTYP", " ", 30, ref data),
				Common.DictionaryValueExtractor("MESTYP", " ", 30, ref data),
				Common.DictionaryValueExtractor("MESCOD", " ", 3, ref data),
				Common.DictionaryValueExtractor("MESFCT", " ", 3, ref data),
				Common.DictionaryValueExtractor("STD", " ", 1, ref data),
				Common.DictionaryValueExtractor("STDVRS", " ", 6, ref data),
				Common.DictionaryValueExtractor("STDMES", " ", 6, ref data),
				Common.DictionaryValueExtractor("SNDPOR", " ", 10, ref data),
				Common.DictionaryValueExtractor("SNDPRT", " ", 2, ref data),
				Common.DictionaryValueExtractor("SNDPFC", " ", 2, ref data),
				Common.DictionaryValueExtractor("SNDPRN", " ", 10, ref data),
				Common.DictionaryValueExtractor("SNDSAD", " ", 21, ref data),
				Common.DictionaryValueExtractor("SNDLAD", " ", 70, ref data),
				Common.DictionaryValueExtractor("RCVPOR", " ", 10, ref data),
				Common.DictionaryValueExtractor("RCVPRT", " ", 2, ref data),
				Common.DictionaryValueExtractor("RCVPFC", " ", 2, ref data),
				Common.DictionaryValueExtractor("RCVPRN", " ", 10, ref data),
				Common.DictionaryValueExtractor("RCVSAD", " ", 21, ref data),
				Common.DictionaryValueExtractor("RCVLAD", " ", 70, ref data),
				Common.DictionaryValueExtractor("CREDAT", " ", 8, ref data),
				Common.DictionaryValueExtractor("CRETIM", " ", 6, ref data),
				Common.DictionaryValueExtractor("REFINT", " ", 14, ref data),
				Common.DictionaryValueExtractor("REFGRP", " ", 14, ref data),
				Common.DictionaryValueExtractor("REFMES", " ", 14, ref data),
				Common.DictionaryValueExtractor("ARCKEY", " ", 70, ref data),
				Common.DictionaryValueExtractor("SERIAL", " ", 20, ref data)
			});
		}
	}
}

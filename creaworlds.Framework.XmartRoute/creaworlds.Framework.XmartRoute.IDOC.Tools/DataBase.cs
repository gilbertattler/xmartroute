﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace creaworlds.Framework.XmartRoute.IDOC.Tools
{
	public static class DataBase
	{
		#region SQLConnect
		public static bool OpenConnection(ref SqlCommand cmd)
		{
			return DataBase.OpenConnection(ref cmd, null);
		}

		public static bool OpenConnection(ref SqlCommand cmd, string connStrKey)
		{
			bool done = false;
			cmd = new SqlCommand();

			try
			{
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DATASERVER"].ConnectionString);
				cmd.Connection.Open();

				done = (cmd.Connection.State == ConnectionState.Open);
			}
			catch (Exception ex)
			{
				Diagnostics.OnError(new Exception("Ha ocurrido un error al tratar de conectar con la base de datos.", ex));
				cmd.Dispose();
				cmd = null;
			}

			return done;
		}
		#endregion

		#region BeginTran
		public static bool BeginTran(ref SqlCommand cmd)
		{
			bool done = true;

			try
			{
				cmd.Transaction = cmd.Connection.BeginTransaction();
			}
			catch (Exception ex)
			{
				done = false;
				Diagnostics.OnError(new Exception("HA OCURRIDO UN ERROR AL TRATAR DE INICIAR UNA TRANSACCIÓN", ex));
			}

			return done;
		}
		#endregion

		#region CommitTran
		public static bool CommitTran(ref SqlCommand cmd)
		{
			bool done = true;

			try
			{
				cmd.Transaction.Commit();
			}
			catch (Exception ex)
			{
				done = false;
				Diagnostics.OnError(new Exception("HA OCURRIDO UN ERROR AL TRATAR DE REALIZAR LAS OPERACIONES SOLICITADAS", ex));
			}

			return done;
		}
		#endregion

		#region RollbackTran
		public static void RollbackTran(ref SqlCommand cmd)
		{
			try
			{
				cmd.Transaction.Rollback();
			}
			catch (Exception)
			{

			}
		}
		#endregion

		#region CloseDBConnection
		public static void CloseConnection(ref SqlCommand cmd)
		{
			try
			{
				cmd.Connection.Close();
			}
			catch (Exception ex)
			{
				Diagnostics.OnError(new Exception("HA OCURRIDO UN ERROR AL TRATAR DE CERRAR LA CONEXIÓN CON LA BASE DE DATOS", ex));
			}
		}
		#endregion
	}
}

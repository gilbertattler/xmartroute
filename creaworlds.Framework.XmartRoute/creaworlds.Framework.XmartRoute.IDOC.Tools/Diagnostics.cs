﻿using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace creaworlds.Framework.XmartRoute.IDOC.Tools
{
	public static class Diagnostics
	{
		public static void OnError(Exception error)
		{
			try
			{
				string data = Common.Serializer(error);
				string path = ConfigurationManager.AppSettings["PATH_LOG"];
				string file = string.Format("{0}\\{1}.xml", path, DateTime.Now.Ticks);

				using (FileStream fs = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
				{
					using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
					{
						sw.Write(data);
					}
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex);
				throw error;
			}
		}
	}
}

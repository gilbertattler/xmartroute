﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace creaworlds.Framework.XmartRoute.IDOC.Tools
{
	public static class Common
	{
		#region Serializer
		/// <summary>
		/// Obtiene el XML correspondiente a un objeto
		/// </summary>
		/// <param name="sender">Objeto a Cambiar</param>
		/// <returns>Cadena con el XML generado</returns>
		public static string Serializer(object sender)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				XmlSerializer xmlSerializer = new XmlSerializer(sender.GetType());
				xmlSerializer.Serialize(ms, sender);
				ms.Seek(0, SeekOrigin.Begin);

				using (StreamReader sr = new StreamReader(ms))
				{
					return sr.ReadToEnd();
				}
			}
		}
		#endregion

		#region ValueExtractor
		public static string DictionaryValueExtractor(string key, string repeater, int times, ref Dictionary<string, string> data)
		{
			string result = string.Empty;
			if (data != null && data.ContainsKey(key) && data[key] != null)
			{
				result = data[key].Trim();
            }

			result = result + new StringBuilder().Insert(0, repeater, result.Length - times).ToString();
			return result;
		}
		#endregion

	}
}

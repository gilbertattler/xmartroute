﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.XmartRoute.WebApi.Interfaces.Requests
{
	public interface IRequest
	{
		string UserName { get; set; }
		string Password { get; set; }
		string Signature { get; set; }
	}
}

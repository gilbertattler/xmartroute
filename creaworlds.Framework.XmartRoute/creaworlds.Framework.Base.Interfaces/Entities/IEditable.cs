﻿using System;

namespace creaworlds.Framework.Base.Interfaces.Entities
{
	public interface IEditable
	{
		string ID { get; set; }
		string CreatorUserID { get; set; }
		string CreatorUserName { get; set; }
		string ModifierUserID { get; set; }
		string ModifierUserName { get; set; }
		DateTime? CreatedDate { get; set; }
		DateTime? ModifiedDate { get; set; }
	}
}

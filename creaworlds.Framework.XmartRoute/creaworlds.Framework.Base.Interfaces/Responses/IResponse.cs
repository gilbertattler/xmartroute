﻿using creaworlds.Framework.Base.Enumerators.Responses;

namespace creaworlds.Framework.Base.Interfaces.Responses
{
	public interface IResponse
	{
		int? CodeNumber { get; set; }
		Codes CodeName { get; set; }
		string Message { get; set; }
		int? ErrorNumber { get; set; }

		void Configure(Codes codeName);
		void Configure(Codes codeName, string message, int? errorNumber);

		void Configure(int codeNumber);
		void Configure(int codeNumber, string message, int? errorNumber);
	}
}

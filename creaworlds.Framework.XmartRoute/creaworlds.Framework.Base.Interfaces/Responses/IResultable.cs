﻿
namespace creaworlds.Framework.Base.Interfaces.Responses
{
	public interface IResultable<obj>
	{
		obj Result { get; set; }
	}
}

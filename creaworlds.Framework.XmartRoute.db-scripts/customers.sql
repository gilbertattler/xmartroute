﻿-- drop table cliente_datos;
CREATE TABLE cliente_datos (
	cliente_datos_id int IDENTITY(1,1) NOT NULL,
	cliente_datos_key uniqueidentifier NOT NULL,
	cliente_datos_clave varchar(50) NOT NULL,
	cliente_datos_nombre varchar(255) NOT NULL,
	cliente_datos_rsocial varchar(255) NOT NULL,
	cliente_datos_rfc varchar(50) NOT NULL,
	cliente_datos_domicilio varchar(150) NOT NULL,
	cliente_datos_colonia varchar(150) NOT NULL,
	cliente_datos_ciudad varchar(150) NOT NULL,
	cliente_datos_estado varchar(150) NOT NULL,
	cliente_datos_pais varchar(150) NOT NULL,
	cliente_datos_zipcode varchar(10) NOT NULL,
	cliente_datos_credito money NOT NULL,
	cliente_datos_deuda money NOT NULL,
	cliente_datos_disponible money not null,
	cliente_datos_activo bit NOT NULL,
	cliente_datos_borrado bit NOT NULL,
	constraint pk_cliente_datos primary key (cliente_datos_id),
	constraint uk_cliente_datos_key unique (cliente_datos_key),
);

create index ix_cliente_datos_clave on cliente_datos (cliente_datos_clave) include (cliente_datos_id, cliente_datos_key);
go

-- drop table cliente_sucursal;
CREATE TABLE cliente_sucursal (
	cliente_sucursal_id int IDENTITY(1,1) NOT NULL,
	cliente_sucursal_key uniqueidentifier NOT NULL,
	cliente_datos_id int not null,
	cliente_sucursal_nombre varchar(255) NOT NULL,
	cliente_sucursal_domicilio varchar(150) NOT NULL,
	cliente_sucursal_colonia varchar(150) NOT NULL,
	cliente_sucursal_zipcode varchar(10) NOT NULL,
	cliente_sucursal_ciudad varchar(150) NOT NULL,
	cliente_sucursal_estado varchar(150) NOT NULL,
	cliente_sucursal_pais varchar(150) NOT NULL,
	cliente_sucursal_refer varchar(255) not null,
	cliente_sucursal_visita tinyint NOT NULL,
	cliente_sucursal_activo bit NOT NULL,
	cliente_sucursal_borrado bit NOT NULL,
	constraint pk_cliente_sucursal primary key (cliente_sucursal_id),
	constraint uk_cliente_sucursal_key unique (cliente_sucursal_key),
	constraint fk_cliente_sucursal_cliente_datos_id foreign key (cliente_datos_id) references cliente_datos (cliente_datos_id)
);
go


go
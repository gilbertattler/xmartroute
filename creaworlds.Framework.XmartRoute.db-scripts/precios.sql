update inventario_datos
set inventario_datos_virtual = producto_presentacion_id,
	inventario_datos_disponible = producto_presentacion_id
go

begin tran;
	delete from producto_precio;
	
	declare cur4sucursales cursor fast_forward for
	select empresa_datos_id, empresa_sucursal_id
	from empresa_sucursal AS es;

	open cur4sucursales;
	declare @empresaid smallint, @sucursalid int;
	fetch next from cur4sucursales into @empresaid, @sucursalid;

	while @@FETCH_STATUS = 0 begin
		declare cur4listas cursor fast_forward for
		select cp.catalogo_precio_id
		from catalogo_precio AS cp
		where cp.catalogo_precio_id != 0;
		
		open cur4listas;
		declare @listaid smallint;
		fetch next from cur4listas into @listaid;
		
		while @@FETCH_STATUS = 0 begin
			declare cur4articulos cursor fast_forward for
			select 
				pp.producto_datos_id, pp.producto_presentacion_id, 
				convert(money, right(pp.producto_presentacion_clave, 3)) as costo, 
				convert(money, right(pp.producto_presentacion_clave, 3)) * convert(money, 1 + (0.18/convert(float, @listaid))) as venta
			from producto_presentacion AS pp;
			
			open cur4articulos;
			declare @productoid int, @articuloid int, @costo money, @venta money;
			fetch next from cur4articulos into @productoid, @articuloid, @costo, @venta;
			
			while @@FETCH_STATUS = 0 begin
				insert into producto_precio (
					empresa_datos_id, empresa_sucursal_id, producto_datos_id, producto_presentacion_id, catalogo_precio_id,
					producto_precio_costo, producto_precio_base, producto_precio_impuestos, producto_precio_total, producto_precio_margen,
					producto_precio_activo, producto_precio_borrado)
				values(@empresaid, @sucursalid, @productoid, @articuloid, @listaid, @costo, @venta, 0, @venta, ROUND(((@venta / @costo)-1) * 100, 2), 1, 0);
				
				fetch next from cur4articulos into @productoid, @articuloid, @costo, @venta;
			end
			
			close cur4articulos;
			deallocate cur4articulos;
			
			fetch next from cur4listas into @listaid;
		end
		
		close cur4listas;
		deallocate cur4listas;
		
		fetch next from cur4sucursales into @empresaid, @sucursalid;
	end

	close cur4sucursales;
	deallocate cur4sucursales;
	
	select *
	from producto_precio;
commit tran;
go

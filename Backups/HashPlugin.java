/**
 * 
 * Phonegap Hash plugin
 * 
 * Version 1.0
 * 
 * Abdeslam Aabidi 2014
 * 
 * Email: xinony@gmail.com
 * 
 */
 
package com.abdsoft.Hash;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * This class Hash a string called from JavaScript.
 */
public class HashPlugin extends CordovaPlugin 
{
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException 
    {
        try
        {
            JSONObject params = args.getJSONObject(0);
            String data = params.getString("data");
            String hash = params.getString("hash").toLowerCase(); // md5, sha1, sha-256, sha-384, sha-512
            
            if (action.equals("hashString")) 
            {
                callbackContext.success(hashString(data, hash));
                return true;
            }
            else if (action.equals("hashFile"))
            {
                callbackContext.success(hashFile(data, hash));
                return true;
            }
        }
        catch (Exception e)
        {
            callbackContext.success(e.getMessage());
        }
        
        return false;
    }

    public boolean isMDAvailable(String hash)
    {
        if(!hash.equals("uuid")) {
            try
            {
                MessageDigest.getInstance(hash);
            }
            catch(NoSuchAlgorithmException x)
            {
                 return false;
            }
        }
        return true;
    }
    
    private String hashString(String data, String hash) throws NoSuchAlgorithmException, IOException 
    {
        if(isMDAvailable(hash) == false) 
        {
            return "Invalid Hash algorithm: " + hash;
        }
        
        if(hash.equals("uuid")) {
            return UUID.randomUUID().toString();
        } else {
            MessageDigest md = MessageDigest.getInstance(hash);
            //md.reset();
            //md.update(data.getBytes("UTF-8"));
            
            //byte[] bytes = md.digest(); 
            //byte[] bytes = md.digest(data.getBytes("UTF-16LE"));
            byte[] bytes = md.digest(data.getBytes("UTF-8"));
            String result = new BigInteger(1, bytes).toString(16).toUpperCase();
            
            if(result.length() < 32) {
                result = "0" + result;
            }

			return result;
        }
    }
    
    private String hashFile(String data, String hash) throws NoSuchAlgorithmException, IOException 
    {       
        if(isMDAvailable(hash) == false) 
        {
            return "Invalid Hash algorithm.";
        }
        
        File file = new File(data);
        if (!file.exists())
        {
            return "File does not exist.";
        }
        
        MessageDigest md = MessageDigest.getInstance(hash);
        md.reset();
        
        InputStream is = new FileInputStream(file);
        byte[] buffer = new byte[8192];
        int read = 0;
        
        while((read = is.read(buffer)) > 0)
        {
            md.update(buffer, 0, read);
        }
        is.close();
        
        byte[] bytes = md.digest();
        String result = new BigInteger(1, bytes).toString(16).toUpperCase();
        
        if(result.length() < 32) {
			result = "0" + result;
        }

        return result;
    }    
}